<?php
require_once realpath(dirname(__FILE__) . '/..') . "/models/Job.php";

if(!empty($_FILES["datoteka1"]["name"])) {
$jb         = new Job;

        $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/konektum/public/uploads/";
        $target_file = $target_dir . basename($_FILES["datoteka1"]["name"]);
        $target_file = preg_replace('/\s+/', '', $target_file);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $Filename=basename($_FILES["datoteka1"]["name"]);
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["datoteka1"]["size"] > 100000000000000000) {   //velicinu??provjerit!!!
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
           && $imageFileType != "gif" && $imageFileType !="pdf" && $imageFileType != "docx" && $imageFileType != "doc" && $imageFileType != "txt") {
            echo "Sorry, only JPG, JPEG, PNG, GIF, PDF and DOCX and DOC files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["datoteka1"]["tmp_name"], $target_file)) {
                //Writes the information to the database
                $fke=$_SESSION['user_session'];
                $stmt = $jb->con->prepare("select max(id) as id from job where fk_employer=$fke");
                $stmt->execute();
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $idf = $row['id'];
                }
                $stmt = $jb->con->prepare("UPDATE job SET file=:file WHERE fk_employer=:fke and id=:id");
                $stmt->bindParam(":file", $Filename);
                $stmt->bindParam(":fke", $fke);
                $stmt->bindParam(":id", $idf);
                if($stmt->execute()) {
                    echo "The file ".$Filename. " has been uploaded.";
                }
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
}else {
    echo "no files";
}
    
?>