<?php

require_once "dbconf.php";
require_once 'sanitize.php';
require_once 'sendmail.php';

if(isset($_POST['submitp'])) {
    $ime = sanitize($_POST['ime']);
    $prezime = sanitize($_POST['prezime']);
    $nazivpoduzeca = sanitize($_POST['nazivpoduzeca']);
    $grad = sanitize($_POST['grad']);
   // $podrucjerada = sanitize($_POST['podrucje']);
    $email = sanitize($_POST['email']);
    $password = sha1(sanitize($_POST['passwordp']));

    try {
        $stmt = $db_con->prepare("SELECT * FROM employer WHERE email=:email");
        
        $stmt->execute(array(":email"=>$email));
        $count = $stmt->rowCount();

        if($count==0){

            $stmt = $db_con->prepare("INSERT INTO employer (first_name,last_name,company_name,email,password,city,fk_job) VALUES(:ime, :prezime, :nazivpoduzeca, :email, :password, :grad, :image)");
            $stmt->bindParam(":ime",$ime);
            $stmt->bindParam(":prezime",$prezime);
            $stmt->bindParam(":nazivpoduzeca",$nazivpoduzeca);
            $stmt->bindParam(":grad",$grad);
            $image="blankuser.jpg";
            $stmt->bindParam(":image",$image);
            $stmt->bindParam(":email",$email);
            $stmt->bindParam(":password",$password);
    
            if($stmt->execute()) {
                $stmt = $db_con->prepare("Select id from employer where email=:email");
                $stmt->bindParam(":email", $email);
                if($stmt->execute()) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    $id = $row['id'];
                    $mail = new Mail;
                    $mail->sendConfirmationEmail($email, 'Molimo potvrdite vas email', $id, true);
                }
                echo "registered";
            } else {
                echo "Query could not execute !";
            }
        } else {
            echo "1";
        }
    }
    catch(PDOException $e){
        echo $e->getMessage();
    }
}

?>
