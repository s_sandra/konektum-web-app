<?php
class Mail {
    private $transport;
    private $mailer;
    function __construct() {
        require_once "vendor/swiftmailer/swiftmailer/lib/swift_required.php";
        $this->transport = Swift_SmtpTransport::newInstance('mail.konektum.com', 25)
                   ->setUsername('konektum') // Your Gmail Username
                   ->setPassword('Workit123'); 
        $this->mailer = Swift_Mailer::newInstance($this->transport);
    }
    public function sendMail($to, $subject, $body) {
        $message = Swift_Message::newInstance($subject)
                 ->setFrom('konektum@konektum.com') 
                 ->setTo(array($to => $to))
                 ->setBody($body, 'text/html');

        if ($this->mailer->send($message)) {
        } else {
			echo "faile";
        }
    }
    public function sendConfirmationEmail($to, $subject, $userID, $isEmployer = false) {
        if (!$isEmployer) {
            $link = 'www.konektum.com/konektum/public/Get/activate/Student/'. md5($userID);
            $body = "Kako bi potvrdili vašu registraciju kliknite na link ispod <br><a href='$link'>Konektum</a><br> Ukolike se registracija ne izvrši klikom na link pošaljite ponovno zahtjeve <br> <a href = 'www.konektum.com'>Vaš Konektum</a>\n\n Ako email ne radi molimo vas iskoristite ovaj link \n".$link;
            $this->sendMail($to, $subject, $body);
        } else {
            $link = 'www.konektum.com/konektum/public/Get/activate/Employer/'. md5($userID);
            $body = "Kako bi potvrdili vašu registraciju kliknite na link ispod \n <a href='$link'>Konektum</a>\n Ukolike se registracija ne izvrši klikom na link pošaljite ponovno zahtjeve \n <a href = 'www.konektum.com'>Vaš Konektum</a>\n\n Ako email ne radi molimo vas iskoristite ovaj link \n".$link;
            $this->sendMail($to, $subject, $body);
        }
    }
}
?>
