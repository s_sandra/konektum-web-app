<?php
require_once realpath(dirname(__FILE__) . '/..') . "/core/dbconf.php";
require_once realpath(dirname(__FILE__) . '/..') . "/core/login.php";
require_once realpath(dirname(__FILE__) . '/..') . "/models/Employer.php";

if(is_array($_FILES)) {
    if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
        $sourcePath = $_FILES['userImage']['tmp_name'];
        $temp = explode(".", $_FILES["userImage"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . "/konektum/public/uploads/". $newfilename;
        $fke=$_SESSION['user_session'];
        $jb         = new Employer;


        $stmt = $jb->con->prepare("select fk_job from employer WHERE id=:fke");
        $stmt->bindParam(":fke", $fke);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $file = $_SERVER['DOCUMENT_ROOT'] . "/konektum/public/uploads/".$row['fk_job'];
            if (file_exists($file && $file!="blankuser.png")) {
                unlink($file);
            }
        }

        
         if(move_uploaded_file($sourcePath,$targetPath)) {
        //Writes the information to the database
            $stmt = $jb->con->prepare("UPDATE employer SET fk_job=:file WHERE id=:fke");
            $stmt->bindParam(":file", $newfilename);
            $stmt->bindParam(":fke", $fke);
            $stmt->execute();
            echo '<img src="/konektum/public/uploads/'.$newfilename.'" width="300px" height="300px"/>';
        }
    }
}
?>