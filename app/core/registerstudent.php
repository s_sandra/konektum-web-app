<?php

require_once realpath(dirname(__FILE__) . '/..') . "/models/Job.php";
require_once  "dbconf.php";
require_once 'sanitize.php';
require_once 'sendmail.php';
if(isset($_POST['submits'])) {
    $ime = $_POST['ime'];
    $prezime = $_POST['prezime'];
    $studij = $_POST['studij'];
    $godinastudija = $_POST['godinastudija'];
    $godinaupisa = $_POST['godinaupisa'];
    $boraviste=$_POST['boraviste'];
    $hobi = $_POST['hobi'];
    $iskustvo = $_POST['iskustvo'];
    $projekti = $_POST['projekti'];
    $email = $_POST['email'];
    $password = sha1(sanitize($_POST['password']));
    $kategorija ="PrirodneZnanosti";
    $jb = new Job;
    $kategorija = $jb->getCategoryId($kategorija);
    $fk_empl = "blankuser.jpg"; 

    try {
        $stmt = $db_con->prepare("SELECT * FROM student WHERE email=:email");
        $stmt->execute(array(":email"=>$email));
        $count = $stmt->rowCount();

        if($count==0){

            $stmt = $db_con->prepare("INSERT INTO student(first_name,last_name,university,current_year,year_of_study,email,password,domicle,hobby,experience,projects,fk_category,fk_employer) VALUES(:ime, :prezime, :studij, :godinastudija, :godinaupisa, :email, :password, :boraviste, :hobi, :iskustvo, :projekti, :fk_cat, :fk_empl)");
            $stmt->bindParam(":ime",$ime);
            $stmt->bindParam(":prezime",$prezime);
            $stmt->bindParam(":studij",$studij);
            $stmt->bindParam(":godinastudija",$godinastudija);
            $stmt->bindParam(":godinaupisa",$godinaupisa);
            $stmt->bindParam(":boraviste",$boraviste);
            $stmt->bindParam(":hobi",$hobi);
            $stmt->bindParam(":iskustvo",$iskustvo);
            $stmt->bindParam(":projekti",$projekti);
            $stmt->bindParam(":fk_cat",$kategorija);
            $stmt->bindParam("fk_empl", $fk_empl);
            
            $stmt->bindParam(":email",$email);
            $stmt->bindParam(":password",$password);
            

            if($stmt->execute()) {
                $stmt = $db_con->prepare("Select id from student where email=:email");
                $stmt->bindParam(":email", $email);
                if($stmt->execute()) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    $id = $row['id'];
                    $mail = new Mail;
                    $mail->sendConfirmationEmail($email, 'Molimo potvrdite vas email', $id);
                }
                echo "registered";
            } else {
                echo "Query could not execute !";
            }
        } else {
            echo "1";
        }
    }
    catch(PDOException $e){
        echo $e->getMessage();
    }
}

?>
