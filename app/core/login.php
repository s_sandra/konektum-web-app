<?php
@session_start();
require_once 'dbconf.php';
require_once 'sanitize.php';
if(isset($_POST['login'])) {
    $user_email = sanitize(trim($_POST['email']));
    $user_password = sanitize(trim($_POST['pass']));
    try {
        $stmt = $db_con->prepare("SELECT * FROM student WHERE email=:email");
        $stmt->execute(array(":email"=>$user_email));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $count = $stmt->rowCount();
       
        if(sha1($user_password) == $row['password']) {
            echo "okstudent"; // log in
            $_SESSION['user_session'] = $row['id'];
        } else {
             $stmt = $db_con->prepare("SELECT * FROM employer WHERE email=:email");
             $stmt->execute(array(":email"=>$user_email));
             $row = $stmt->fetch(PDO::FETCH_ASSOC);
             $count = $stmt->rowCount();
             if(sha1($user_password) == $row['password']) {
                echo "okposlodavac"; // log in
                $_SESSION['user_session'] = $row['id'];
              }
              else {
                  echo 400;
              }
        }
    }
    catch(PDOException $e){
        echo $e->getMessage();
    }
}
?>
