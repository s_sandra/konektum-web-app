<?php
if (!isset($_SESSION))
  {
    session_start();
  }
session_destroy();

unset($_SESSION['username']);

header('Location: /konektum/');
?>
