<?php 
class Controller {
    public static function model($model) {
        if (file_exists('../app/models/'. $model .'.php')) {
            require_once '../app/models/'. $model .'.php';
            return new $model();
        } else {
            echo "file doesnt exist";
        }
    }
    
    public function view($view, $data = []) {
        require_once '../app/views/'.$view.'.html';
    }
}