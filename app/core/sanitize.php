<?php

function sanitize($what) {
    return strip_tags($what);
}