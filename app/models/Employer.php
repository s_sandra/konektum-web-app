<?php 
require_once "Model.php";
require_once realpath(dirname(__FILE__) . '/..') . "/core/sendmail.php";
class Employer extends Model {
  
    public function getName() {
            if (isset($_SESSION['user_session'])) {
                $id = $_SESSION['user_session'];   
                $query = $this->con->prepare('SELECT first_name FROM employer WHERE id = :uid');
                $query->bindValue(':uid', $id, PDO::PARAM_INT);
                $query->execute();
                
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    return $row['first_name'];
                }
        }
    }
    
    public function getEmployersJobs(){
          if (isset($_SESSION['user_session'])) {
            $fkemp = $_SESSION['user_session'];
            $query = $this->con->prepare("select  * from `job` where fk_employer=:fkemp");
            $query->bindValue(':fkemp', $fkemp, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $idoglasa=$row['id'];
                echo '<tr>';
                echo '<td><h5>'. $row['name'] . '</h5></td>';
                echo '<td><h5>'. $row['location'] . '</h5></td>';
                echo '<td><h5>'. $row['date'] . '</h5></td>';
                echo '<td><button class="btn my-btn-primary" style="color:white; margin-top:4px;" onclick="obrisioglas('.$idoglasa.')">OBRIŠI</button>
                          <button class="btn my-btn-primary" style="color:white; margin-top:4px;" data-toggle="modal" data-target="#portfolioModalprofilposla" onclick="prikaziprofilposlasaizmjenom('.$idoglasa.')">IZMIJENI</button></a><td>';
                echo '</tr>';  
            }
          }
    }
    
    public function settings(){
            $id = $_SESSION['user_session'];
            $query = $this->con->prepare("select * from `employer` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                echo '<script>$(document).ready(function() {
                    $.fn.editable.defaults.mode = "inline";  
                    $("#ime").editable({
                        success: function(response){
                            $.ajax({
                                type: "GET",
                                //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
                                url: "Get/name/Employer",
                                dataType: "html",
                                success: function (response) {
                                $("#odgimekorisnikaposlodavca").html(response);
                                  }
                            });    
                        }  
                      });
                    $("#prezime").editable();
                    $("#firma").editable();
                    $("#email").editable();
                    $("#lozinka").editable();
                    $("#grad").editable();
                    
                    ajaxOptions : {
                      type : "get"
                    }
                   
                    });</script>
                  <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
                            <script>
                               $(document).ready(function() {
                                    $(function(){
                                        $("#fil1").change(function(){
                                            $("#uploadForm").submit();
                                             });
                                        });
                                        
                                $("form#uploadForm").submit(function(){
                                    var formData = new FormData(this);
                                     $.ajax({
                                         url: "/konektum/app/core/uploadp.php",
                                         type: "POST",
                                         data: formData,
                                         async: false,
                                         success :  function(data)
                                            {  
                                           $("#targetLayer").html(data);
                                            },
                                        cache: false,
                                        contentType: false,
                                        processData: false  
                                      });
                                      return false;
                                 });     
                                 });  
                            </script>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <form id="uploadForm" action="uploadp.php" method="post">
                                <div id="targetLayer" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2"><span class="btn btn-primary btn-file">
                                 učitaj logo <input type="file" id="fil1"" name="userImage"/>
                                </span></div>
                            </form>
                            </div>
                            
                         </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <div class="form-group col-lg-12 col-md-12">
                               <div class="row">
                               <div class="col-lg-6">
                               <h4>Ime:</h4>
                               </div>
                               <div class="col-lg-6">
                               <a href="#" id="ime" data-type="text" data-pk="'.$row['id'].'" data-url="Get/EmployerFunction/EditFirstName" data-placement="right" data-title="Unesite ime:"><h5>'.$row['first_name'].'</h5></a><br><br>
                               </div>
                               </div>
                               </div>
                               <div class="form-group col-lg-12 col-md-12">
                               <div class="row">
                               <div class="col-lg-6">
                               <h4>Prezime:</h4>
                               </div>
                               <div class="col-lg-6">
                               <a href="#" id="prezime" data-type="text" data-pk="'.$row['id'].'" data-url="Get/EmployerFunction/EditLastName" data-placement="right" data-title="Unesite prezime:"><h5>'.$row['last_name'].'</h5></a><br><br>
                               </div>
                               </div>
                               </div>
                               <div class="form-group col-lg-12 col-md-12">
                               <div class="row">
                               <div class="col-lg-6">
                               <h4>Naziv poduzeća:</h4>
                               </div>
                               <div class="col-lg-6">
                               <a href="#" id="firma" data-type="text" data-pk="'.$row['id'].'" data-url="Get/EmployerFunction/EditCompanyName" data-placement="right" data-title="Unesite naziv poduzeća:"><h5>'.$row['company_name'].'</h5></a><br><br>
                               </div>
                               </div>
                               </div>
                               <div class="form-group col-lg-12 col-md-12">
                               <div class="row">
                               <div class="col-lg-6">
                               <h4>Grad:</h4>
                               </div>
                               <div class="col-lg-6">
                               <a href="#" id="grad" data-type="text" data-pk="'.$row['id'].'" data-url="Get/EmployerFunction/EditCity" data-placement="right" data-title="Unesite grad:"><h5>'.$row['city'].'</h5></a><br><br>
                               </div>
                               </div>
                               </div>
                               </div>
                         </div>
                         </div><br><br><br>
                         <div class="row">      
                               <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <div class="row">
                               <div class="col-lg-6">
                               <h4>E-mail:</h4>
                               </div>
                               <div class="col-lg-6">
                               <a href="#" id="email" data-type="text" data-pk="'.$row['id'].'" data-url="Get/EmployerFunction/EditEmail" data-placement="right" data-title="Unesite e-mail:"><h5>'.$row['email'].'</h5></a>
                               </div>
                               </div>
                               </div>
                               <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                               <div class="row">
                               <div class="col-lg-6">
                               <h4>Lozinka:</h4>
                               </div>
                               <div class="col-lg-6">
                               <a href="#" id="lozinka" data-type="text" data-pk="'.$row['id'].'" data-url="Get/EmployerFunction/EditPassword" data-placement="right" data-title="Unesite lozinku:"><h5>unesite novu lozinku</h5></a>
                               </div>
                               </div>
                               </div>
                         </div>
                  </div><br>
                    ';
            } 
    }
    
    public function EditFirstName(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `employer` set first_name=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }    
       public function EditLastName(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `employer` set last_name=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
       public function EditCompanyName(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `employer` set company_name=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }  
       public function EditEmail(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `employer` set email=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
        public function EditPassword(){
            $pk = $_POST['pk'];
            $password= password_hash(sanitize($_POST['value']), PASSWORD_DEFAULT);
            $query = $this->con->prepare("update `employer` set password=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $password, PDO::PARAM_INT);
            $query->execute();
       } 
        public function EditCity(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `employer` set city=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }
        public function ProfilePicture1(){
          if (isset($_SESSION['user_session'])) {
                $id = $_SESSION['user_session'];
                $query = $this->con->prepare('SELECT fk_job FROM employer WHERE id = :uid');
                $query->bindValue(':uid', $id, PDO::PARAM_INT);
                $query->execute();
              
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    echo '<img src="/konektum/public/uploads/'.$row['fk_job'].'" class="img-rounded img-responsive myimage" width="300px" height="300px"/>';
                }
            }
         
      }     
    public function activate($activationCode) {
        $query = $this->con->prepare("select id from employer");
        $query ->execute();
        while($row = $query->fetch(PDO::FETCH_ASSOC)) {
            if (md5($row['id'] == $activationCode)) {
                $id = $row['id'];
                if (md5($id) == $activationCode) {
                    $query = $this->con->prepare('update employer set active="Yes" where id =:id');
                    $query->bindValue(':id', $id, PDO::PARAM_INT);
                    if($query->execute()) {
                        header("Location: /konektum/public/index.php");
                    }
                    else {
                        echo "failed";
                    }
                }

            }
            else {
                echo "None match";
            }
        }
    }
    public function isActivated() {
        if(isset($_SESSION['user_session'])) {
            $id = $_SESSION['user_session'];
            $query = $this->con->prepare("select active from employer where id=:id");
            $query->bindValue(":id", $id, PDO::PARAM_INT);
            $query->execute();
            while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                return $row['active'];
            }
        }
    }
    public function contact($email) {
        if (isset($_SESSION['user_session'])) {
            $query = $this->con->prepare("select first_name, last_name, email, city, company_name from employer where id=:id");
            $query->bindValue(":id", $_SESSION['user_session'], PDO::PARAM_INT);
            $query->execute();
            $body = "";
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $body = "Poštovani<br> Poslodavac " . $row['first_name'] . " " . $row['last_name'] . " iz " . $row['company_name']. ", " . $row['city'] . " odabrao vas je kao potencijalnog kandidata za posao. <br> Ukoliko želite više informacija molimo vas da ga kontaktirate na sljedećoj email adresi: ". $row['email']; 
            }
            $m = new Mail;
            $m->sendMail($email, "Konektum - Obavijest o poslu", $body);
        }
    }
}
