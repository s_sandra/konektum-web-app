<?php 
require_once "Model.php";

class Job extends Model {
   
    public function getCategoryId($which) {
            $query = $this->con->prepare("select `id` from category where `name`=:name");
            $query->bindValue(':name', $which, PDO::PARAM_STR);
            $query->execute();
            $id = NULL;
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                  return $id = $row['id'];

            }
    }
   

     public function jobData($which) {
        if (isset($_SESSION['user_session'])) { 
            $id = $which;
            $query = $this->con->prepare("select `id`, `name`, `description`, `location`, `date`  from `job` where fk_category=:fk");
            $query->bindValue(':fk', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $idposla = $row['id'];
                echo "<tr>";
                echo "<td><h5>" . $row['name'] . "</h5></td>";
               // echo "<td>" . $row['description'] . "</td>";
                echo "<td><h5>" . $row['location'] . "</h5></td>";
                echo "<td><h5>" . $row['date'] . "</h5></td>";
                echo '<td><a href="#portfolioModalprofilposla" style="color:black;" class="portfolio-link" data-toggle="modal" onclick="prikaziprofilposla('.$idposla.')"><h5>VIŠE</h5></a><td>';
                echo "</tr>";   
            }
            
        }
            
    }
  
  
     public function Profil($which) {
            $id = $which;
            $query = $this->con->prepare("select  * from `job` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
              $fkemployer=$row['fk_employer'];  
            }
            $query = $this->con->prepare("select  * from `employer` where id=:id");
            $query->bindValue(':id', $fkemployer, PDO::PARAM_INT);
            $query->execute();
            $email = "";
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
              echo '<div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img src="/konektum/public/uploads/'.$row['fk_job'].'" class="img-rounded img-responsive myimage" width="200px" height="200px"/>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h5>Naziv poslodavca:&nbsp</h5><h4>'.$row['company_name'].'</h4>
                    </div>
                    ';
              $email = $row['email'];
            }
            $query = $this->con->prepare("select  * from `job` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                echo '
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <br>
                <h5>Naziv posla:&nbsp</h5><h4> '.$row['name'].'</h4>
                </div>
                </div>
                </div>
                 ';
                $name = $row['name'];
                $func ="kontaktirajPoslodavca('$email','$name')";
                echo '<div class="row">
                <div class="col-lg-12"><h4>Opis posla: </h4></div>
                <div class="col-lg-12"><h5> '.$row['description'].'</h5></div></div>';
                 if(!empty($row['file'])){
                    $file = $row['file'];
                    echo '<div class="row"><div class="col-lg-12"><h4><a href="/konektum/app/core/download.php?download_file='.$file.'">Klikni i preuzmi više detalja o poslu</a></h4></div></div><br>';
                 }
                echo'<button type="button" class="btn btn-primary" data-type="toggle" title="Kontaktira poslodavca putem Konektum maila" onclick ='.$func.'>Prijavi se</button>
                '; 
                
            } 
            
         }
         
       public function IzmijeniJobs($which) {
            $id = $which;
            $query = $this->con->prepare("select  * from `job` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                echo '<script>$(document).ready(function() {
                    $.fn.editable.defaults.mode = "inline";  
                    $("#nazivposla").editable({
                        success: function(response, newValue){
                            $.ajax({
                            type: "GET",
                            url: "Get/Dataemployersjobs/Employer",
                            dataType: "html",
                            success: function (response) {
                            $("#prikazposlovaposlodavca").html(response);
                              }
                             });  
                        }  
                      });
                    $("#opisposla").editable({
                        success: function(response, newValue){
                            $.ajax({
                            type: "GET",
                            url: "Get/Dataemployersjobs/Employer",
                            dataType: "html",
                            success: function (response) {
                            $("#prikazposlovaposlodavca").html(response);
                              }
                             });  
                        } 
                    });
                    $("#lokacija").editable({
                        success: function(response, newValue){
                            $.ajax({
                            type: "GET",
                            url: "Get/Dataemployersjobs/Employer",
                            dataType: "html",
                            success: function (response) {
                            $("#prikazposlovaposlodavca").html(response);
                              }
                             });  
                        } 
                    });
                    ajaxOptions : {
                      type : "get"
                    }
                   
                    });
</script>
                    <div class="row">
                    <div class="col-lg-6">
                    <h4><span>Naziv posla:</span></h4>
                    </div>
                    <div class="col-lg-6">
                    <a href="#" id="nazivposla" data-type="text" data-pk="'.$row['id'].'" data-url="Get/JobFunction/EditName" data-placement="right" data-title="Unesite novi naziv posla:"><h5>'.$row['name'].'</h5></a><br><br>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-6">
                    <h4><span>Opis posla:</span></h4>
                    <h5><span>(max 4000 znakova)</span></h5>
                    </div>
                    <div class="col-lg-6">
                    <a href="#" id="opisposla" data-type="textarea" data-pk="'.$row['id'].'" data-url="Get/JobFunction/EditDescription" data-placement="right" data-title="Unesite novi opis posla:"><h5>'.$row['description'].'</h5></a><br><br>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-6">
                    <h4><span>Lokacija:</span><h4>
                    </div>
                    <div class="col-lg-6">
                    <a href="#" id="lokacija" data-type="text" data-pk="'.$row['id'].'" data-url="Get/JobFunction/EditLocation" data-placement="right" data-title="Unesite novu lokaciju posla:"><h5>'.$row['location'].'</h5></a>

                    </div>
                    </div>
                    ';
                if(!empty($row['file'])){
                    $file = $row['file'];
                    echo '<br><br><div class="row"><div class="col-lg-12"><h5><a href="/konektum/app/core/download.php?download_file='.$file.'">Preuzmite učitane detalje o poslu</a></h5></div></div><br>';
                    echo '<h4>Ili učitajte novu datoteku:</h4>';
                    echo '<span hidden id="skriven">'.$id.'</span>';
                 }
           }
       }
       
       public function EditName(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `job` set name=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }    
       public function EditDescription(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `job` set description=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
       public function EditLocation(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `job` set location=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
     
     public function DeleteJobs($which) {
            $id = $which;
            $query = $this->con->prepare("delete from `job` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
                  
     }
    
}