<?php 
require_once "Model.php";
require_once realpath(dirname(__FILE__) . '/..') . "/core/sendmail.php";

class Student extends Model {

    public function getName() {
            if (isset($_SESSION['user_session'])) {
                $id = $_SESSION['user_session'];   
                $query = $this->con->prepare('SELECT first_name FROM student WHERE id = :uid');
                $query->bindValue(':uid', $id, PDO::PARAM_INT);
                $query->execute();
                
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    return $row['first_name'];
                }
            }
       }
        
    public function getStudents($universityName) {
            if (isset($_SESSION['user_session'])) {
                $id = $_SESSION['user_session'];   
                $query = $this->con->prepare('SELECT id, first_name, last_name, current_year, hobby FROM student where university = :university');
                $query->bindValue(':university', $universityName);
                $query->execute();
                
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    $idstudenta=$row['id'];
                    $slika = $this->getStatus($row['id']) == 'Da' ? "<img src='/konektum/public/img/yes.png' width='20px' height='20px'>" : "<img src='/konektum/public/img/no.png' width='20px' height='20px'>";
               
                    echo '<tr>';
                    echo '<td><h5>'. $slika . '</h5></td>';
                    echo '<td><h5>'. $row['first_name'] . '</h5></td>';
                    echo '<td><h5>'. $row['last_name'] . '</h5></td>';
                    echo '<td><h5>'. $row['current_year'] . '</h5></td>';
                    echo '<td><h5>'. $row['hobby'] . '</h5></td>';
                    $status = $this->getStatus($idstudenta);
                    echo '
                    <td>
                    <a href="#portfolioModalprofilstudenta" style="color:black;" class="portfolio-link" data-toggle="modal" onclick="prikaziprofilstudenta('.$idstudenta.')"><h5><b>VIŠE</b></h5></a>
                    </td>';
                    echo '</tr>';
                  
                }
            }
       }    
        
    public function Profil($which) {
            $id = $which;
            $query = $this->con->prepare("select * from `student` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                echo '
               <div  class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
                            <script>
                               $(document).ready(function() {   
                                    $.ajax({
                                         type: "GET",
                                         //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
                                         url: "Get/SlikaProfila/Student/+'.$row['id'].'+",
                                         dataType: "html",
                                         success: function (response) {
                                         $("#targetLayer").html(response);
                                         }
                                      }); 
                                 });  
                            </script>
                           <div id="targetLayer" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>   
                           </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h5>Ime:&nbsp</h5></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4>'.$row['first_name'].'</h4></div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h5>Prezime:&nbsp</h5></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4>'.$row['last_name'].'</h4></div>
                            </div>
                            </div>';
                           
                            $uniid=$row['university'];
                            $query = $this->con->prepare("select name from `universities` where id=:id");
                            $query->bindValue(':id', $uniid);
                            $query->execute();
                            $uni=$query->fetchColumn();          
                            if ($uniid == 15) {
                                $uni = "Visoka poslovna škola PAR";
                            }          
                            
                       echo'<div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h5>Fakultet: &nbsp</h5></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4>'.$uni.'</h4></div>
                            </row>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h5>Grad:&nbsp</h5></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4>'.$row['domicle'].'</h4></div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h5>Godina studija:&nbsp</h5></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4>'.$row['current_year'].'</h4></div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h5>Godina upisa:&nbsp</h5></div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <h4>'.$row['year_of_study'].'</h4></div>
                            </div>
                            </div>  
                         </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>Stečene vještine, znanja i interesi:&nbsp</h4>
                    <h5>'.$row['hobby'].'</h5>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>Radno iskustvo:&nbsp</h4>
                    <h5>'.$row['experience'].'</h5>
                    </div>
                  <div class="row">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>Radovi i projekti:&nbsp</h4>
                    <h5>'.$row['projects'].'</h5><br>
                    </div>
                  </div>
                 <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Kontaktira studenta putem Konektum emaila" onclick = kontaktirajStudenta("'.$row['email'].'")>Kontaktiraj</button> 
               </div>';
              
            }   
        
    }
    
    public function settings(){
            $id = $_SESSION['user_session'];
            
            $query = $this->con->prepare("select * from `student` where id=:id");
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $slika = $this->getStatus($row['id']) == 'Da' ? "<img src='/konektum/public/img/yes.png' width='20px' height='20px'>" : "<img src='/konektum/public/img/no.png' width='20px' height='20px'>";
                echo '<script>$(document).ready(function() {
                    $.fn.editable.defaults.mode = "inline";  
                    $("#ime").editable({
                        success: function(response){
                            $.ajax({
                                type: "GET",
                                //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
                                url: "Get/name/Student",
                                dataType: "html",
                                success: function (response) {
                                $("#odgimekorisnikastudenta").html(response);
                                  }
                            });    
                        }  
                      });
                    $("#prezime").editable();
                    $("#fakultet").editable({
                            value: 1,    
                          source: [
                             {value: 1, text: "Akademija primjenjenih umjetnosti"},
                             {value: 2, text: "Filozifski fakultet"},
                             {value: 3, text: "Građevinski fakultet"},
                             {value: 4, text: "Medicinski fakultet"},
                             {value: 5, text: "Odjel za biotehnologiju"},
                             {value: 6, text: "Odjel za fiziku"},
                             {value: 7, text: "Odjel za informatiku"},
                             {value: 8, text: "Odjel za matematiku"},
                             {value: 9, text: "Pomorski fakultet"},
                             {value: 10, text: "Pravni fakultet"},
                             {value: 11, text: "Tehnički fakultet"},
                             {value: 12, text: "Učiteljski fakultet"},
                             {value: 13, text: "Razno"}
                              ]
                            }
                    );
                    $("#godinastudija").editable();
                    $("#godinaupisa").editable();
                    $("#email").editable();
                    $("#lozinka").editable();
                    $("#grad").editable();
                    $("#hobi").editable();
                    $("#iskustvo").editable();
                    $("#projekti").editable();
                   
                    $("#dostupnost").click(function () { 
                        $.ajax({
                                url: "Get/toggleStatus/'.$row['id'].'",
                                type: "Get",
                                success :  function(data){  
                                    $("#dostupnost").html(data);
                                },        
                        });
                    });
                   
                   
                   
                   
                    ajaxOptions : {
                      type : "get"
                    }
                   
                    }); </script>
                  <div class="row">
                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
                            <script>
                               $(document).ready(function() {
                                    $(function(){
                                        $("#fil1").change(function(){
                                            $("#uploadForm").submit();
                                             });
                                        });
                                        
                                $("form#uploadForm").submit(function(){
                                    var formData = new FormData(this);
                                     $.ajax({
                                         url: "/konektum/app/core/upload.php",
                                         type: "POST",
                                         data: formData,
                                         async: false,
                                         success :  function(data)
                                            {  
                                           $("#targetLayer").html(data);
                                            },
                                        cache: false,
                                        contentType: false,
                                        processData: false  
                                      });
                                      return false;
                                 });     
                                 });  
                            </script>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <form id="uploadForm" action="upload.php" method="post">
                                <div id="targetLayer" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div>
                                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2""><span id="glupigumb" class="btn btn-primary btn-file">
                                 odaberi sliku <input type="file" id="fil1" name="userImage"/>
                                </span></div>
                            </form>
                            </div>
                            </div>
                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Ime:</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="ime" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditFirstName" data-placement="right" data-title="Unesite ime:"><h5>'.$row['first_name'].'</h5></a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Prezime:</h4>
                            </div>
                            <div class=col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="prezime" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditLastName" data-placement="right" data-title="Unesite prezime:"><h5>'.$row['last_name'].'</h5></a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Fakultet:</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="fakultet" data-type="select" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditUniversity" data-placement="right" data-title="Odaberite fakultet:"><h5>Odaberi fakultet</h5></a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Grad:</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="grad" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditDomicle" data-placement="right" data-title="Unesite grad:"><h5>'.$row['domicle'].'</h5></a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Dostupan:</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="dostupnost" data-type="text" data-pk="'.$this->getStatus($row['id']).'" data-url="/konektum/public/Student/toggleStatus/'.$row['id'].'" data-placement="right" data-title="Unesite status:"><h5>'. $slika .'</h5></a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Godina studija:</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="godinastudija" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditCurrentYear" data-placement="right" data-title="Unesite godinu studija:"><h5>'.$row['current_year'].'</h5></a>
                            </div>
                            </div>
                            </div>
                            <div class="form-group col-lg-12 col-md-12">
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <h4>Godina upisa:</h4></div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                            <a href="#" id="godinaupisa" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditYearofStudy" data-placement="right" data-title="Unesite godinu upisa:"><h5>'.$row['year_of_study'].'</h5></a>
                            </div>
                            </div>
                            </div>  
                         </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4>E-mail:</h4></div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    <a href="#" id="email" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditEmail" data-placement="right" data-title="Unesite e-mail:"><h5>'.$row['email'].'</h5></a>
                    </div>
                    </div>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4>Lozinka:</h4></div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                    <a href="#" id="lozinka" data-type="text" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditPassword" data-placement="right" data-title="Unesite novu lozinku:"><h5>unesite novu lozinku</h5></a>
                    </div>
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>Stečene vještine i kompetencije te interesi:</h4>
                    <a href="#" id="hobi" data-type="textarea" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditHobby" data-placement="right" data-title="Unesite hobi:"><h5>'.$row['hobby'].'</h5></a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>Radno iskustvo:</h4>
                    <a href="#" id="iskustvo" data-type="textarea" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditExperience" data-placement="right" data-title="Unesite radno iskustvo:"><h5>'.$row['experience'].'</h5></a>
                    </div>
                  <div class="row">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h4>Radovi i projekti:</h4>
                    <a href="#" id="projekti" data-type="textarea" data-pk="'.$row['id'].'" data-url="Get/StudentFunction/EditProjects" data-placement="right" data-title="Unesite radove i projekte:"><h5>'.$row['projects'].'</h5></a><br>
                    </div>
                  </div>
                    ';
              
            } 
    }
    
    public function EditFirstName(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set first_name=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
            
       }    
       public function EditLastName(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set last_name=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
       
       public function EditUniversity(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set university=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }  
       public function EditCurrentYear(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set current_year=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
        public function EditYearofStudy(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set year_of_study=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       } 
        public function EditEmail(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set email=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }
       public function EditPassword(){
            $pk = $_POST['pk'];
            $password= password_hash(sanitize($_POST['value']), PASSWORD_DEFAULT);
            $query = $this->con->prepare("update `student` set password=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $password, PDO::PARAM_INT);
            $query->execute();
       }
       public function EditDomicle(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set domicle=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }
       public function EditHobby(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set hobby=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }
       public function EditExperience(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set experience=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }
       public function EditProjects(){
            $pk = $_POST['pk'];
            $value = $_POST['value'];
            $query = $this->con->prepare("update `student` set projects=:value where id=:id");
            $query->bindValue(':id', $pk, PDO::PARAM_INT);
            $query->bindValue(':value', $value, PDO::PARAM_INT);
            $query->execute();
       }
      
      public function ProfilePicture($idstudenta){  //slika na poslodavcu kad gleda profile studenta
          if (isset($_SESSION['user_session'])) {
                $id = $idstudenta; //ulogiran je poslodavac
                $query = $this->con->prepare('SELECT fk_employer FROM student WHERE id = :uid');
                $query->bindValue(':uid', $id, PDO::PARAM_INT);
                $query->execute();
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    echo '<img src="/konektum/public/uploads/'.$row['fk_employer'].'" class="img-rounded img-responsive myimage" width="300px" height="300px"/>';
                }
            }
         
      }
    
    public function ProfilePicture1(){  //slika na postavkama studenta
          if (isset($_SESSION['user_session'])) {
                $id = $_SESSION['user_session']; //ulogiran je poslodavac
                $query = $this->con->prepare('SELECT fk_employer FROM student WHERE id = :uid');
                $query->bindValue(':uid', $id, PDO::PARAM_INT);
                $query->execute();
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    echo '<img src="/konektum/public/uploads/'.$row['fk_employer'].'" class="img-rounded img-responsive myimage" width="300px" height="300px"/>';
                }
            }
         
      }

    public function activate($activationCode) {
        $query = $this->con->prepare("select id from student");
        $query ->execute();
        while($row = $query->fetch(PDO::FETCH_ASSOC)) {
            if (md5($row['id'] == $activationCode)) {
                $id = $row['id'];
                if (md5($id) == $activationCode) {
                    $query = $this->con->prepare('update student set active="Yes" where id =:id');
                    $query->bindValue(':id', $id, PDO::PARAM_INT);
                    if($query->execute()) {
                        header("Location: /konektum/public/index.php");
                    }
                    else {
                        echo "failed";
                    }
                }

            }
            else {
                echo "None match";
            }
        }
    }
    public function isActivated() {
        if(isset($_SESSION['user_session'])) {
            $id = $_SESSION['user_session'];
            $query = $this->con->prepare("select active from student where id=:id");
            $query->bindValue(":id", $id, PDO::PARAM_INT);
            $query->execute();
            while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                return $row['active'];
            }
        }
    }
    public function contact($email, $job) {
        if (isset($_SESSION['user_session'])) {
            $query = $this->con->prepare("select * from student where id=:id");
            $query->bindValue(":id", $_SESSION['user_session'], PDO::PARAM_INT);
            $query->execute();
            $body = "";
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
               $uniid=$row['university'];
               $query = $this->con->prepare("select name from `universities` where id=:id");
               $query->bindValue(':id', $uniid);
               $query->execute();
               $uni=$query->fetchColumn();          
               if ($uniid == 15) {
                     $uni = "Visoka poslovna škola PAR";
               }          
                            
                $body = "Poštovani<br><br> Student " . $row['first_name'] . " " . $row['last_name'] . " iz " . $row['domicle'] . " prijavio se na vaš oglas za posao ". $job. ".<br> 
                Ukoliko želite više informacija molimo vas da ga kontaktirate na sljedećoj email adresi: ". $row['email'] . "
                    <br/><br/><b>Detaljni podaci:</b><br/>
                    Ime:<i> " . $row['first_name'] . " </i><br>
                    Prezime:<i> " . $row['last_name'] . " </i><br>
                    Boravište: <i> " . $row['domicle'] . "</i> <br>
                    Fakultet:<i> ". $uni ." <br>
                    Godina studija:<i> " . $row['current_year'] . "</i> <br>
                    Godina upisa:<i> " . $row['year_of_study'] . "</i> <br>
                    Stečene vještine i kompetencije:<i> " . $row['hobby'] . " </i><br>
                    Radno iskustvo: <i>".$row['experience']."</i> <br>
                    Radovi i projekti: <i>".$row['projects']."</i><br>
                "; 
            }
            $m = new Mail;
            $m->sendMail($email, "Konektum - Obavijest o prijavi na oglas", $body);
        }
    }
    
    public function getStatus($id) {
            $query = $this->con->prepare("select taken from student where id=:id");
            $query->bindValue(":id",$id, PDO::PARAM_INT);
            $query->execute();
             while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                return $row['taken'];
             }

    }
    
    public function toggleStatus($id) {
        
            $query = $this->con->prepare("select taken from student where id=:id");
            $query->bindValue(":id",$id, PDO::PARAM_INT);
            $query->execute();
            while($row = $query->fetch(PDO::FETCH_ASSOC)) {
                 $status = $row['taken'];
            }

            $newStatus = "";
            if($status == "Ne") {
                $newStatus = "Da";
            } else {
                $newStatus = "Ne";
            }
            $query = $this->con->prepare('update student set taken=:taken where id =:id');
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->bindValue(':taken', $newStatus);
            $query->execute();
            echo $newStatus == "Da" ? "<img src='/konektum/public/img/yes.png' width='20px' height='20px'>" : "<img src='/konektum/public/img/no.png' width='20px' height='20px'>";
         }
}

