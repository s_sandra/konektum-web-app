<?php

class Get extends Controller{
    private $stu = NULL;
    private $emp = NULL;
    private $jo = NULL;
    private $uni = NULL;
    public function __construct (){    
        $this->stu = $this->model("Student");
        $this->emp = $this->model("Employer");
        $this->jo = $this->model("Job");
        $this->uni = $this->model("Universities");
    }

    private function select($which) {

        if ($which == "Student") {
            return  $this->stu;
        } else if ($which =="Employer") {
            return $this->emp;
        } else if ($which =="Job"){
            return $this->jo;
        } else {
            return $this->uni;
        }
    }
    public function name($whose) {   //za oba modela->imena
        echo $this->select($whose)->getName();
        exit;
    }
    
    public function Profili($which, $forWhat) {   //za oba modela->profili
        $this->select($which)->Profil($forWhat);
        exit;
    }
    
   public function Izmjenaprofila($which) {  //za studenta ->postavke
        $this->select($which)->settings();
        exit;
   }
   
   public function SlikaProfila($which,$forWhat) {  //student->profilna slika ali iz pogleda poslodavca
        $this->select($which)->ProfilePicture($forWhat);
        exit;
   }
   
    public function SlikaProfilap($which) {  //poslodavac i student->profilna slika 
        $this->select($which)->ProfilePicture1();
        exit;
   }
   
   public function EmployerFunction($fname) {  //funckije employera(izmijena postavki)
        $this->emp->$fname();
        exit;
    }
    
   public function StudentFunction($fname) {  //funckije studenta(izmijena postavki)
        $this->stu->$fname();
        exit;
    }
    
    public function universities($fname, $whichUni) {  //studenti kategorije
        $this->stu->$fname($whichUni);
        exit;
    }
    
    public function Data($which, $forWhat) {   //poslovi kategorije
        $this->select($which)->jobData($forWhat);
        exit;
    }
    
     public function Dataemployersjobs($whose) {  //poslovi tog poslodavca
        $this->select($whose)->getEmployersJobs();
        exit;
    }

    public function JobFunction($fname, $forWhat) {  //funckije poslova(izmijeni i izbrisi)
        $this->jo->$fname($forWhat);
        exit;
    }
    public function activate($whose, $activationCode) { 
        $this->select($whose)->activate($activationCode);
        exit;
    }

    public function isActivated($whose) {
        echo trim($this->select($whose)->isActivated());
        exit;
    }
    public function contact($email) {
        $this->emp->contact($email);
        exit;
    }
    public function contactEmployer($email, $job) {
        $this->stu->contact($email, $job);
        exit;
    }
    
    public function getStatus($id) {
        $this->stu->getStatus($id);
        exit;
    }
    public function toggleStatus($id) {
        $this->stu->toggleStatus($id);
        exit;
    }
}
?>
