$('document').ready(function()
{

  $("#register-form-student").validate({
      rules:
   {
   ime: {
   required: true,
   minlength: 3
   },
   prezime: {
   required: true,
   minlength: 3
   },
   studij: {
   required: true,
   },
   godinastudija: {
   required: true,
   },
   godinaupisa: {
   required: true,
   },
   boraviste: {
   required: true,
   },
   hobi: {
   required: true,
   },
   iskustvo: {
   required: true,
   },
   projekti: {
   required: true,
   },
   password: {
   required: true,
   minlength: 8,
   maxlength: 30
   },
   cpassword: {
   required: true,
   equalTo: '#password'
   },
   email: {
   required: true,
   email: true
   },
    },
       messages:
    {
            ime: {
              required: "Molimo vas da unesete vaše ime.",
              minlength: "Ime treba imati barem 3 znaka."
            },
            prezime: {
              required: "Molimo vas da unesete vaše prezime.",
              minlength: "Prezime treba imati barem 3 znaka."
            },
            studij: {
              required: "Molimo vas da odaberete vaš studij.",
            },
            godinastudija: {
              required: "Molimo vas da unesete godinu vašeg studija.", 
            },
            godinaupisa: {
              required: "Molimo vas da unesete godinu upisa vašeg studija.", 
            },
            hobi: {
              required: "Molimo vas da unesete vaše interese.", 
            },
            iskustvo: {
              required: "Molimo vas da unesete vaša iskustva.", 
            },
            projekti: {
              required: "Molimo vas da unesete vaše projekte.", 
            },
            boraviste: {
              required: "Molimo vas da unesete vaš grad.", 
            },
            password:{
              required: "Molimo vas da unesete vašu lozinku.",
              minlength: "Lozinka treba imati barem 8 znakova."
            },
            email: {
              required: "Molimo vas da unesete vašu e-mail adresu.",
              email: "Email treba biti u formatu example@example.com",
            },
            cpassword:{
              required: "Ponovno unesite vašu lozinku.",
              equalTo: "Lozinke nisu jednake."
       }
       },
    submitHandler: submitForm
       });
    function submitForm()
    {
    var data = $("#register-form-student").serialize();

    $.ajax({

    type : 'POST',
    url  : '/konektum/app/core/registerstudent.php',
    data : data,
    beforeSend: function()
    {

    },
    success :  function(data)
         {
      
        if(data==1){
          $("#portfolioModalneuspjesnaregistacija").modal("show");
        }
        else if(data=="registered")
        {
          $("#portfolioModaluspjesnaregistacija").modal("toggle");
          $("#zatvori").click(function(){
            $("#portfolioModalsvi").modal("hide");
          });
        }
        else{
           $("#portfolioModalneuspjesnaregistacija").modal("toggle");
        }
         }
    });
    return false;
  }

});
