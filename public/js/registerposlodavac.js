$('document').ready(function()
{

  $("#register-form-poslodavac").validate({
      rules:
   {
   ime: {
   required: true,
   minlength: 3
   },
   prezime: {
   required: true,
   minlength: 3
   },
   passwordp: {
   required: true,
   minlength: 8,
   maxlength: 30
   },
   cpasswordp: {
   required: true,
   equalTo: '#passwordp'
   },
   email: {
   required: true,
   email: true
   },
    },
       messages:
    {
            ime: {
              required: "Molimo vas da unesete vaše ime.",
              minlength: "Ime treba imati barem 3 znaka."
            },
            prezime: {
              required: "Molimo vas da unesete vaše prezime.",
              minlength: "Prezime treba imati barem 3 znaka."
            },
            passwordp:{
                  required: "Molimo vas da unesete vašu lozinku.",
                 minlength: "Lozinka treba imati barem 8 znakova."
                     },
            email: {
              required: "Molimo vas da unesete vašu e-mail adresu.",
              email: "Email treba biti u formatu example@example.com",
            },
           cpasswordp:{
              required: "Ponovno unesite vašu lozinku.",
              equalTo: "Lozinke nisu jednake."
       }
       },
    submitHandler: submitForm
       });
    function submitForm()
    {
    var data = $("#register-form-poslodavac").serialize();

    $.ajax({

    type : 'POST',
    url  : '/konektum/app/core/registerposlodavac.php',
    data : data,
    beforeSend: function()
    {

    },
    success :  function(data)
 
         {
       $("#molimocekajte").append("<b>Molimo pričekajte.</b>");
        if(data==1){
          $("#portfolioModalneuspjesnaregistacija").modal("show");
        }
        else if(data=="registered")
        {
         
          $("#portfolioModaluspjesnaregistacija").modal("toggle");
         
          $("#zatvori").click(function(){
            $("#portfolioModalsvi").modal("hide");
          });
        }
        else{
          $("#portfolioModalneuspjesnaregistacija").modal("toggle");
        }
         }
    });
    return false;
  }

});
