     $(document).ready(function() {
         
        //PRIBAVI IME trenutno ulogiranog 
$.ajax({
    type: "GET",
    //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
    url: "Get/name/Employer",
    dataType: "html",

    success: function (response) {
        $("#odgimekorisnikaposlodavca").html(response);

    }
});

//pribavi sve poslove poslodavca
$.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/Dataemployersjobs/Employer",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovaposlodavca").html(response);
        }
    });


         
         
         
     $.ajax({
         type: "GET",
         url:"Get/isActivated/Employer",
         dataType: "html",
         success: function(r) {
         if (r === "No") {
              $("#portfolioModalpotvrda").modal("show");
         }
      }});
     
 });
  
  
  
  function kontaktirajStudenta(email) {
      $.ajax({
      type: "GET",
      url: "Get/contact/" + email,
      dataType: "html",
      success: function (r) {
      //alert("poslan mail");
      $("#portfolioModalposlanmail").modal("show");
      }
      });
      }


function prikaziprofilstudenta($idstudenta){
     $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/Profili/Student/"+$idstudenta+"/",
        dataType: "html",
        success: function (response) {
            $("#prikazpodatakaostudentu").html(response);
        }
    });
}

function prikaziprofilposlasaizmjenom($idoglasa){
     $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/JobFunction/IzmijeniJobs/"+$idoglasa+"/",
        dataType: "html",
        success: function (response) {
            $("#prikazpodatakaoposlu").html(response);
        }
    });
}

function prikazpostavki(){
     $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/Izmjenaprofila/Employer/",
        dataType: "html",
        success: function (response) {
            $("#prikazpostavki").html(response);
             $.ajax({
                type: "GET",
                //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
                url: "Get/SlikaProfilap/Employer",
                dataType: "html",
                success: function (response) {
                         $("#targetLayer").html(response);
                     }
            });
        }
    });
}

function obrisioglas($idoglasa){
     $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/JobFunction/DeleteJobs/"+$idoglasa+"/",
        dataType: "html",
        success: function (response) {
            $.ajax({
            type: "GET",
            url: "Get/Dataemployersjobs/Employer",
            dataType: "html",
            success: function (response) {
            $("#prikazposlovaposlodavca").html(response);
        }
    });
        }
    });
}

     

//IT POSLOVI
function prikaziStudenteAkademija() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/1/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovaakademija").html(response);
        }
    });
}

function prikaziStudenteFilozofski() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/2/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovafilozofski").html(response);
        }
    });

}
function prikaziStudenteGradjevinski() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/3/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovagradevinski").html(response);
        }
    });     
}
function prikaziStudenteMedicinski() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/4/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovamedicinski").html(response);
        }
    });     
}

function prikaziStudenteBiotehnologija() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/5/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovabiotehnologija").html(response);
        }
    });
}
function prikaziStudenteFizika() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/6/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovafizika").html(response);
        }
    });
}
function prikaziStudenteInformatika() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/7/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovainformatika").html(response);
        }
    });
}
function prikaziStudenteMatematika() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/8/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovamatematika").html(response);
        }
    });
}

function prikaziStudentePomorski() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/9/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovapomorski").html(response);
        }
    });
}

function prikaziStudentePravni() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/10/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovapravni").html(response);
        }
    });
}

function prikaziStudenteTehnicki() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/11/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovatehnicki").html(response);
        }
    });
}
function prikaziStudenteUciteljski() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/12/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovauciteljski").html(response);
        }
    });
}

function prikaziStudenteEkonomija() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/14/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovaekonomija").html(response);
        }
    });
}

function prikaziStudentePar() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/15/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovapar").html(response);
        }
    });
}

function prikaziStudenteRazno() {
    $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/universities/getStudents/13/",
        dataType: "html",
        success: function (response) {
            $("#prikazposlovaostalo").html(response);
        }
    });
}
  