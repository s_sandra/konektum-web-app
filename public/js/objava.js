$('document').ready(function()
{
    
$("#objavaposla").validate({
      rules:
   {
   nazivposla: {
   required: true,
   minlength: 3,
   maxlength: 350,
   },
   opisposla: {
   required: true,
   minlength: 5,
   maxlength: 4000,
   },
   kategorija: {
   required: true,
       }
    },
       messages:
    {
            nazivposla: {
              required: "Molimo vas da unesete naziv posla.",
              minlength: "Naziv posla treba imati barem 3 znaka.",
              maxlength: "Naziv posla nemože imati više od 350 znakova",
            },
            opisposla:{
              required: "Molimo vas da unesete opis posla.",
              minlength: "Opis posla treba sadrzavati najmanje 5 znakova.",
              maxlength: "Opis posla nemože imati više od 4000 znakova, molimo vas učitajte dokument.",
                     },
            kategorija: {
              required: "Molimo vas da odaberete kategoriju."
            }
       }
   });
    
$("form#objavaposla").submit(function(){

    var formData = new FormData(this);
    
    $.ajax({
        url: '/konektum/app/controllers/Post.php',
        type: 'POST',
        data: formData,
        async: false,
        success :  function(response)
        {  
        $.ajax({
        type: "GET",
        //IME_KONTROLERA/IME_FUNKCIJE/PRVI_ARGUMENT/DRUGI_ARGUMENT
        url: "Get/Dataemployersjobs/Employer",
        dataType: "html",
        success: function (response) {
            $("#portfolioModal7").modal("hide");
            $("#prikazposlovaposlodavca").html(response);
        }
         });
       
         },
        cache: false,
        contentType: false,
        processData: false  
    });

    return false;
});
    $("form#izmjenaposla").submit(function(){
    var formData = new FormData(this);
    formData.append('datoteka1', $("#datoteka1")[0].files[0]);
    formData.append('id', $("#skriven").text());
    $.ajax({
        url: '/konektum/app/core/izmjena.php',  //novi post1.php napravit!!
        type: 'POST',
        data: formData,
        async: false,
        success :  function(response)
        {  
            alert(response);
        
        },
        cache: false,
        contentType: false,
        processData: false 
    });

    return false;
    
   }); 
    
});      
   
