<?php 
require_once('../app/init.php');
$app = new App;  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Konektum je novi oglasnik za traženje studentske prakse, posla ili savršenog kandidata za vaše projekte!">
    <meta name="author" content="Konektum">
	<meta name="google-site-verification" content="sAfThkUBHC884I2Zln5tJpBuHj2137VG7OHUZPO8mWg" />
	<meta name="google-site-verification" content="sAfThkUBHC884I2Zln5tJpBuHj2137VG7OHUZPO8mWg" />
	<link rel="icon" href="favicon.ico" type="image/x-icon" />

    <title>Konektum | Pronađi posao, praksu ili savršenog kandidata! </title>
    
     <!-- You can use open graph tags to customize link previews.
    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
	<meta property="og:url"           content="http://konektum.com" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Konektum" />
	<meta property="og:description"   content="Konektum je novi oglasnik za traženje studentske prakse, posla ili savršenog kandidata za vaše projekte!" />
	<meta property="og:image"         content="http://www.konektum.com/path/image.jpg" />
        
    <!-- Bootstrap Core CSS -->
    <link href="/konektum/public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/konektum/public/css/agency.css" rel="stylesheet">
    <link href="/konektum/public/css/full-slider.css" rel="stylesheet">
    <link href="/konektum/public/css/profilepic.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/konektum/public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
   
    
</head>

<body id="page-top" style="background-color:#22313f;">
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRWBC"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WLRWBC');</script>
    <!-- End Google Tag Manager -->
<!--<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=162410474131121";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>-->
 
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
       <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top" ><img src="/konektum/public/img/Untitled-8.png"  margin-top="-90" /></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <!-- <li>
                        <a href="#portfolioModalsvi" class="page-scroll,portfolio-link" data-toggle="modal">Blog</a>
                    </li>-->
                    <li>
                        <a href="#portfolioModalsvi" class="page-scroll,portfolio-link" data-toggle="modal">Registracija</a>
                    </li>
                    <li>
                        <a href="#portfolioModal9" class="page-scroll,portfolio-link" data-toggle="modal">Prijava</a>
                    </li>
                </ul>
            </div>
        </div>
</nav>
<header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url(stu.jpg);">
                    <div class="row">
                    <div class="intro-text center-block">
                        <div class="intro-heading">Student si i tražiš <br>posao u struci?
                        <div style="color:#e74c3c">Pronađi posao! </div></div>
                        <!--<a href="profilestudent.html" class="page-scroll btn btn-xl">Pronađi posao</a>-->
                    </div>
                    </div>
                </div>
                <div class="carousel-caption">
                    <!--<h2>Caption 1</h2>-->
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url(startup-photos.jpg);">
                    <div class="row">

                    <div class="intro-text text-center">
                        <div class="intro-heading">Želite proširiti svoj</br> tim?
                        <div style="color:#e74c3c;">Pronađite kandidate!</div></div>
                        <!--<a href="#portfolio1" class="page-scroll btn btn-xl">Pronađi kandidate</a>-->
                    </div>
                    </div>
                </div>
                <div class="carousel-caption">
                   <!-- <h2>Caption 2</h2>-->
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url(tim.jpg);">
                    <div class="row">
                    <div class="intro-text center-block">
                        <div class="intro-heading">Tražite kandidate </br>za posao? 
                        <div style="color:#e74c3c;">Objavite posao!</div></div>
                    </div>
                    </div>
                </div>
                <div class="carousel-caption">
                    <!--<h2>Caption 3</h2>-->
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
</header>

<!-- Services Section ili o nama opis-->             
   <section id="services" style="background-color:white;">
        <div class="container" >
           <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading" style="color:#22313f">O nama</h2>
                    <h3 class="section-subheading text-muted" style="color:#22313f">Konektum.com je web platforma za spajanje poslodavaca i studenata.</h3>
                </div>
            </div>
            <div class="row">
                
                       <!-- <div class="row section-subheading text-muted" style="color:#E74c3c; font-size:large;">-->
                         <div class="col-lg-10 col-lg-offset-1">
                             <div class="row">

                                 <div class="col-lg-12 text-center">
                                     <div class="row section-subheading text-muted" style=" font-size:large;">
                                         <div class="col-sm-4">
                                             <h3 style="color:#22313f;"><b>Tražiš posao?</b></h3><p style="color:#22313f;" class="timeline-body">
                                                 Student si? Nemaš radno iskustvo u struci? Želiš pronaći praksu za vrijeme studiranja?
                                                 Registriraj se kako bi te poslodavci mogli kontaktirati ili kako bi ti mogao pronaći posao
                                                 koji tebi odgovara.
                                                 <br /> <br />
                                             </p>
                                         </div>
                                         <div class="col-sm-4"><img src="logokojipovezuje.png" width="200px" /></div>
                                         <div class="col-sm-4">
                                             <h3 style="color:#22313f;"><b>Tražite kandidate?</b></h3><p style="color:#22313f;" class="timeline-body">
                                                 Poslodavac ste? Nedostaju vam kvalitetni kandidati za posao ili projekt? Pronađite ih kod nas
                                                 i upotpunite svoj tim. Izaberite si kandidata po volji ili čekajte da on kontaktira vaš oglas.
                                                 <br /> <br />
                                             </p>
                                         </div>

                                     </div>
                                     <br><br><br>
                                     <h2 class="section-heading" style="font-size:28px; color:#22313f;">Učinite to brzo, lako i jednostavno uz konektum!</h2>
                                 </div>

                                 <!--<h4>Lajkaj nas i na facebooku!&nbsp</h4><div class="fb-like" data-href="https://www.facebook.com/konektumsite/" data-width="80" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>-->
                             </div>
            </div>
        </div>
    </section>
                
<!-- About Section -->
    <section id="about" style="background-color:#22313f;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"style="color:white">Kako?</h2>
                    <h3 class="section-subheading text-muted"style="color:white">Sve u samo 3 koraka!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/1-01.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading" style="color:white;">Registrirajte se!</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted"  style="color:white;">Registrirajte se kao student ili poslodavac ovisno o tome tražite li posao u struci i praksu ili tražite savršenog kandidata za vaš projekt!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/2-01.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading" style="color:white;">Pretražite!</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="color:white;">Pretražite kandidate ili poslove po kategorijama, pogledajte njihove profile te odaberite oglas ili kandidate koji vam najviše odgovaraju.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/3-01.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    
                                    <h4 class="subheading" style="color:white;">Odaberite!</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted" style="color:white;">Odabrani oglas za posao ili kandidata jednostavno kontaktirajte klikom na gumb o čemu će student/poslodavac biti obaviješten e-mailom!</p>
                                </div>
                            </div>
                        </li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
   
<!-- Contact Section-->
    <section id="contact" style="background-color: white;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading" style="color:#22313f;">Kontaktirajte nas</h2>
                        <h3 class="section-subheading text-muted" style="color:#22313f;">Ukoliko imate pitanja, želite uputiti primjedbu, prijedlog, sugestiju, ideju ili pohvalu slobodno nas kontaktirajte.</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form name="sentMessage" id="contactForm" novalidate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Vaše ime *" id="name" required data-validation-required-message="Molimo vas da unesete vaše ime.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="E-mail *" id="email" required data-validation-required-message="Molimo vas da unesete vašu e-mail adresu.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                    <div class="form-group">
                                        <input type="tel" class="form-control" placeholder="Telefon *" id="phone" required data-validation-required-message="Unesite vaš broj telefona.">
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Poruka *" id="message" required data-validation-required-message="Unesite poruku."></textarea>
                                        <p class="help-block text-danger"></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 text-center">
                                    <div id="success"></div>
                                    <button type="submit" class="btn btn-xl">Pošalji</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
    
 <!--Clients Aside -->
    <aside class="clients" style="background-color:#E74c3c;">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href='http://startup.rijeka.hr/'>
                        <img src="/konektum/public/img/logos/startup.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href='http://www.rijeka.hr'>
                        <img src="/konektum/public/img/logos/rijeka.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href='http://www.par.hr/hr'>
                        <img src="/konektum/public/img/logos/par.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href='http://www.uniri.hr/'>
                        <img src="/konektum/public/img/logos/svri.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>
<footer style="background-color: #22313f;">
        <div class="container"style="background-color: #22313f;">
                <div class="row">
                    <div class="col-md-4">
                        <span class="copyright" style="color:white;">Copyright © 2016 konektum</span>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-inline social-buttons">
                           <!-- <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>-->
                            <li>
                                <a href="https://www.facebook.com/Konektum-999640536739389/"><i class="fa fa-facebook"></i></a>
                            </li>
                            <!--<li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>-->
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-inline quicklinks">
                           <!-- <li>
                                <a href="#">Privacy Policy</a>
                            </li>-->
                            <li>
                                <a href="#uvjetikoristenja" class="portfolio-link" data-toggle="modal" style="color:white;">Uvjeti korištenja</a>
                            </li>
                        </ul>
                    </div>
                </div>
        </div>
</footer>

<!-- Portfolio Modal defaultno za potvrda emaila-->
    <div class="portfolio-modal modal fade" id="uvjetikoristenja" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="">
                        <div class="modal-body">
                            <!-- Project Details Go Here -->
                            <h2>Uvjeti korištenja</h2>
                            <h4>Korištenjem ove stranice korisnik prihvaća sve rizike koji nastaju iz korištenja ove web stranice te prihvaća koristiti 
                                ovu web stranicu isključivo za osobnu uporabu i na vlastitu odgovornost. <b style="color:#E74C3C;">konektum.com</b> u potpunosti se odriče svake odgovornosti koja na bilo koji način može nastati iz, ili je na bilo koji način vezana za korištenje ovih web stranica, za bilo koje radnje korisnika uporabom ili zlouporabom sadržaja ovih web stranica, te za bilo kakvu štetu koja može nastati korisniku ili bilo kojoj trećoj strani u vezi s uporabom ili zlouporabom korištenja sadržaja ove web stranice. 
                                 Uvjeti korištenja primjenjuju se na sve sadržaje web stranica. Radi osiguranja slobodnog korištenja ove web stranice, ovi postupci su neprihvatljivi i strogo zabranjeni:
                                slanje i razmjena sadržaja koji krše postojeće hrvatske i/ili međunarodne zakone, sadržaja koji je uvredljiv, vulgaran, prijeteći, rasistički ili šovinistički te štetan na bilo koji drugi način
                                slanje i razmjena informacija za koje posjetitelj zna ili pretpostavlja da su lažne, a čije bi korištenje moglo nanijeti štetu drugim korisnicima
                                lažno predstavljanje, odnosno predstavljanje u ime druge pravne ili fizičke osobe
                                svjesno slanje i razmjena sadržaja koji sadrži viruse ili slične računalne datoteke ili programe načinjene u svrhu uništavanja ili ograničavanja rada bilo kojeg računalnog softvera i/ili hardvera i telekomunikacijske opreme
                                Zadržavamo pravo ukloniti sadržaj koji smatra nepodobnim ili ne odgovara ovim Uvjetima korištenja. Prihvaćanjem ovih Uvjeta korištenja korisnik se slaže da s primanjem obavijesti, anketa, poruka administratora i drugih sličnih poruka neophodnih za redovito obavještavanje.
                                konektum.hr se obvezuje poštivati anonimnost i privatnost korisnika ovih web stranica te zadržava pravo izmjene sadržaja ovih web stranica te neće biti odgovoran ni za kakve moguće posljedice proizašle iz takvih promjena.
                                konektum.hr zadržava pravo izmjene ovih uvjeta korištenja bez prethodne namjene. O svakoj promjeni ovih uvjeta korištenja korisnici će biti na odgovarajući način i pravovremeno obaviješteni.
                                <b style="color:#E74C3C;">Korištenjem ove web stranice smatra se da su svi korisnici u svakom trenutku upoznati i suglasni s ovim uvjetima korištenja.</b>
                                </h4><br>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i>Zatvori</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
<!-- Portfolio Modal 9 prijavaaaaaaaaaaaaaaaaaaaaaaaaaaa -->
  <div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-content" style=" width:auto">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container col-lg-4 col-lg-offset-4 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <div class="row">
                            <div class="modal-body" style=" width:auto">
                                <h2>Prijava</h2>
                                <h4>Molimo vas da ispunite tražene podatke.</h4><br>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <form name="sentMessage" id="contactForm2">
                                            <div class="row">
                                                <div class="form-group">
                                                    <input type="email" name="email" class="form-control" placeholder="E-mail *" id="email" required data-validation-required-message="Molimo vas da unesete vašu e-mail adresu.">
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="pass" class="form-control" placeholder="Lozinka *" id="pass" required data-validation-required-message="Molimo vas da unesete vašu lozinku.">
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="col-lg-12 text-center">
                                                    <button name="login" type="submit" class="btn btn-xl" id="linkaj">Pošalji</button>
                                                    </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      
<!-- Portfolio Modal 8 registracijaaaaaaaaaaaaaaaaaaaaaaaa za svee -->
<div class="portfolio-modal modal fade" id="portfolioModalsvi" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
<div class="container">
    <div id="content col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>Registracija</h2><br>
        
            <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">

                
                    <li class="active"><a href="#student" data-toggle="tab"><h4> Kao student</h4></a></li>
               
             
                    <li><a href="#poslodavac" data-toggle="tab"><h4>Kao poslodavac</h4></a></li>
              
                
        </ul>
            
        <div id="my-tab-content" class="tab-content">
            <div class="tab-pane active" id="student">   <br><br>
                                <h4>Molimo vas da ispunite tražene podatke.</h4><br>
                                <div class="row">
                                    <div class="col-lg-12 col-md-11 col-md-offset-0.5">
                                        <form name="sentMessage" id="register-form-student">
                                           <div class="row">
                                                    <div class="form-group col-md-4 col-lg-4">
                                                        <label>Ime:</label>
                                                        <input type="text" name="ime" class="form-control" placeholder="Ime *" id="ime">
                                                       
                                                    </div>
                                                    <div class="form-group col-md-4 col-lg-4">
                                                        <label>Prezime:</label>
                                                        <input type="text" name="prezime" class="form-control" placeholder="Prezime *" id="prezime">
                                                        
                                                    </div>
                                                      <div class="form-group col-md-4 col-lg-4">
                                                        <label>Grad:</label>
                                                        <input type="text" name="boraviste" class="form-control" placeholder="Grad * " id="boraviste">
                                                        
                                                    </div>
                                           </div>
                                           <div class="row">
                                                        <div class="form-group col-md-4 col-lg-4">
                                                        <label>Godina studija:</label>
                                                        <input type="text" name="godinastudija" class="form-control" placeholder="Godina studija *" id="godinastudija">
                                                        
                                                        </div>
                                                        <div class="form-group col-md-4 col-lg-4">
                                                        <label>Godina upisa:</label>
                                                        <input type="text" name="godinaupisa" class="form-control" placeholder="Godina upisa *" id="godinaupisa">
                                                        
                                                        </div>
                                                    <div class="form-group col-md-4 col-lg-4">   
                                                    <label>Odaberi fakultet:</label>
                                                    <select class="form-control" name="studij" id="result">
                                                        <option value="1">Akademija primijenjenih umjetnosti</option>
                                                        <option value="2">Filozofski fakultet</option>
                                                        <option value="3">Građevinski fakultet</option>
                                                        <option value="4">Medicinski fakultet</option>
                                                        <option value="5">Odjel za biotehnoligiju</option>
                                                        <option value="6">Odjel za fiziku</option>
                                                        <option value="7">Odjel za informatiku</option>
                                                        <option value="8">Odjel za matematiku</option>
                                                        <option value="9">Pomorski fakultet</option>
                                                        <option value="1">Pravni fakultet</option>
                                                        <option value="11">Tehnički fakultet</option>
                                                        <option value="12">Učiteljski fakultet</option>
                                                        <option value="14">Ekonomski fakultet</option>
                                                        <option value="15">Visoka poslovna škola PAR</option>
                                                        <option value="13">Veleučilište</option>
                                                    </select>
                                                    </div>
                                                    
                                           </div> 
                                           <div class="row">
                                                        <div class="form-group col-lg-12 col-md-12">
                                                        <label>Stečene vještine i kompetencije te interesi:</label>
                                                        <textarea class="form-control" placeholder="Vještine i interesi (vezano uz studij i struku) * " name="hobi"></textarea><br>
                                                        </div>
                                           </div>
                                           <div class="row">
                                                        <div class="form-group col-lg-12 col-md-12">
                                                        <label>Radno iskustvo:</label>
                                                        <textarea class="form-control" placeholder="Radno iskustvo * " name="iskustvo"></textarea><br>
                                                        </div>
                                           </div>
                                           <div class="row">
                                                        <div class="form-group col-lg-12 col-md-12">
                                                        <label>Radovi i projekti:</label>
                                                        <textarea class="form-control" placeholder="Znanstveni radovi ili projekti *" name="projekti"></textarea><br>
                                                        </div>
                                           </div>
                                           <div class="row">
                                                        <div class="form-group col-md-4 col-lg-4">
                                                        <label>E-mail:</label>
                                                        <input type="text" name="email" class="form-control" placeholder="E-mail *" id="email">
                                                        
                                                        </div>
                                                        <div class="form-group col-md-4 col-lg-4">
                                                        <label>Lozinka:</label>
                                                        <input type="password" name="password" class="form-control" placeholder="Lozinka *" id="password">
                                                       
                                                        </div>
                                                        <div class="form-group col-md-4 col-lg-4">
                                                        <label>Ponovite lozinku:</label>
                                                        <input type="password" name="cpassword" class="form-control" placeholder="Ponovite lozinku *" id="cpassword">
                                                        
                                                        </div>
                                           </div><br>
                                           <div class="col-lg-12 text-center">
                                           <button type="submit" name="submits" class="btn btn-xl" id="btn-submit">Pošalji</button>
                                           </div>
                                        </form>
                                      </div>
                                    </div>
                                </div>
                           
                         
           
            
            <div class="tab-pane" id="poslodavac">
                <br><br>
                                <h4>Molimo vas da ispunite tražene podatke.</h4><br>
                                <div class="row">
                                    <div class="col-lg-12 col-md-11 col-md-offset-0.5">
                                        <form name="sentMessage" id="register-form-poslodavac">
                                            <div class="row">
                                                <div class="form-group col-md-6 col-lg-6">
                                                        <label>Ime:</label>
                                                        <input type="text" name="ime" class="form-control" placeholder="Ime *" id="ime">
                                                        
                                                </div>
                                                <div class="form-group col-md-6 col-lg-6">
                                                        <label>Prezime:</label>
                                                        <input type="text" name="prezime" class="form-control" placeholder="Prezime *" id="prezime">
                                                        
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6 col-lg-6">
                                                        <label>Naziv poduzeća:</label>
                                                        <input type="text" name="nazivpoduzeca" class="form-control" placeholder="Naziv poduzeća " id="nazivpoduzeca">
                                                        
                                                </div>
                                                <div class="form-group col-md-6 col-lg-6">
                                                        <label>Grad:</label>
                                                        <input type="text" name="grad" class="form-control" placeholder="Grad " id="grad">
                                                        
                                                </div>
                                            </div>
                                            
                                           <div class="row">
                                                <div class="form-group col-md-4 col-lg-4">
                                                        <label>E-mail:</label>
                                                        <input type="text" name="email" class="form-control" placeholder="E-mail *" id="email">
                                                        
                                                </div>
                                                <div class="form-group col-md-4 col-lg-4">
                                                    <label>Lozinka:</label>
                                                    <input type="password" name="passwordp" class="form-control" placeholder="Lozinka *" id="passwordp">
                                                    
                                                </div>
                                                <div class="form-group col-md-4 col-lg-4"> 
                                                    <label>Ponovite lozinku:</label> 
                                                    <input type="password" name="cpasswordp" class="form-control" placeholder="Ponovite lozinku *" id="cpasswordp">
                                                   
                                                </div>
                                           </div><br>
                                                <div class="col-lg-12 text-center">
                                                    <div id="success"></div>
                                                    
                                                    <button type="submit" name="submitp" class="btn btn-xl"  id="btn-submit">Pošalji</button><br><div id="molimocekajte"></div>
                                                </div>
                                                </form>
                                          </div>
                                    </div>
                
            </div>
        </div>
            
    </div>
        
        </div>
</div>

               
                
                </div>
        </div>

 <!-- Portfolio Modal uspjesna registracija-->
<div class="portfolio-modal modal fade col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="margin-top:50px; margin-bottom:180px;" id="portfolioModaluspjesnaregistacija" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content" style="align-items:center;">
            <div class="close-modal" data-dismiss="modal" id="zatvori">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row" style="margin-top:80px;">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="modal-body" style="padding-top:0px;">
                            <!-- Project Details Go Here -->
                            <h3 style="color:#e74c3c;">Uspješna registracija!</h3>
                            <h3>Molimo vas da potvrdite e-mail adresu!</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
 <!-- Portfolio Modal neuspjesna registracija-->
    <div class="portfolio-modal modal fade col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="margin-top:50px; margin-bottom:180px;" id="portfolioModalneuspjesnaregistacija" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content" style="align-items:center;">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row" style="margin-top:80px;">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="modal-body" style="padding-top:0px;">
                            <!-- Project Details Go Here -->
                            <h3 style="color:#e74c3c;">Neuspješna registracija!</h3>
                            <h3>E-mail adresa već postoji!</h3>
                            <h3>Molimo vas da provjerite još jednom podatke!</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <!-- Portfolio Modal neuspjesanlogin-->
<div class="portfolio-modal modal fade col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12" style=" margin-bottom:-180px" id="portfolioModalneuspjesanlogin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content" style="align-items:center; width:auto">
            <div class="close-modal" data-dismiss="modal" id="zatvori">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row" style="margin-top:80px; ">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="modal-body" style="padding-top:0px; padding-bottom:0px">
                            <!-- Project Details Go Here -->
                            <h3 style="color:#e74c3c;">Prijava nije uspjela!</h3>
                            <h3 style="color:#e74c3c;">Molimo vas ponovo provjerite i ispunite podatke.</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- jQuery -->
        <script src="/konektum/public/js/jquery.js"></script>
        
        <!-- Registration and Login -->
        <script src="http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js"></script>
        <script src="/konektum/public/js/registerstudent.js"></script>
        <script src="/konektum/public/js/registerposlodavac.js"></script>
        <script src="/konektum/public/js/login.js"></script>
      
        
        <!-- Bootstrap Core JavaScript -->
        <script src="/konektum/public/js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="/konektum/public/js/classie.js"></script>
        <script src="/konektum/public/js/cbpAnimatedHeader.js"></script>

        <!-- Contact Form JavaScript -->
        <script src="/konektum/public/js/jqBootstrapValidation.js"></script>
        <script src="/konektum/public/js/contact_me.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="/konektum/public/js/agency.js"></script>
         
     
        
</body>

</html>
