-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 14, 2016 at 12:25 PM
-- Server version: 10.0.22-MariaDB
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `konektum`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `fk_student` int(11) NOT NULL,
  `fk_employer` int(11) NOT NULL,
  `fk_job` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `fk_student`, `fk_employer`, `fk_job`) VALUES
(1, 'IT', 1, 1, 1),
(2, 'Pravo', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

CREATE TABLE IF NOT EXISTS `employer` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(35) NOT NULL,
  `company_name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `city` varchar(20) NOT NULL,
  `fk_job` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`id`, `first_name`, `last_name`, `company_name`, `email`, `password`, `city`, `fk_job`) VALUES
(2, 'asad', 'asdad', 'asdasd', 'test@gmail.com', '$2y$10$BUsdkehABPqxQ6X2s6.ZqevwLUkUQxLZvMYQHcOrfKtk5r8Nm6VJi', 'asdasda', 0),
(3, 'samuel', 'savanovic', 'dosada', 'asd@gmail.com', '$2y$10$eX3wIG6AP.YxlTixRkp81.LWmnOTEQlr5daY4mjTR.V9blykC9lZC', 'Rijeka', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` varchar(600) NOT NULL,
  `location` varchar(40) NOT NULL,
  `date` date NOT NULL,
  `fk_category` int(11) NOT NULL,
  `fk_employer` int(11) NOT NULL,
  `fk_student` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `name`, `description`, `location`, `date`, `fk_category`, `fk_employer`, `fk_student`) VALUES
(1, 'Umiranje od dosade', 'dosada', 'Rijeka', '2016-03-17', 1, 1, 1),
(2, 'baaah', 'daaaa', 'Zg', '2016-03-10', 2, 2, 2),
(4, 'asa', 'hjjhkkkj', 'dodati', '2016-03-13', 1, 0, 0),
(5, 'asa', 'hjjhkkkj', 'dodati', '2016-03-13', 1, 0, 0),
(6, 'asa', 'hjjhkkkj', 'dodati', '2016-03-13', 1, 0, 0),
(7, 'Klanje', 'Dosade', 'dodati', '2016-03-13', 1, 0, 0),
(8, 'Klanje', 'Dosade', 'dodati', '2016-03-13', 1, 0, 0),
(9, 'sranje', 'asdasdas', 'dodati', '2016-03-14', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `university` varchar(40) NOT NULL,
  `current_year` int(11) NOT NULL,
  `year_of_study` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(128) NOT NULL,
  `domicle` varchar(60) NOT NULL,
  `residence` varchar(60) NOT NULL,
  `hobby` varchar(250) NOT NULL,
  `experience` varchar(500) NOT NULL,
  `projects` varchar(500) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `fk_employer` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `first_name`, `last_name`, `university`, `current_year`, `year_of_study`, `email`, `password`, `domicle`, `residence`, `hobby`, `experience`, `projects`, `fk_category`, `fk_employer`) VALUES
(1, 'sadsad', 'asdsa', 'asasd', 123123, '12312', 'test2@gmail.com', '$2y$10$hvS7LUTWdL0FxEw65EuuPulUNXT5UNyUW6QUQ9gtsYLiDdMiBq24e', 'sadsadsd', 'IMPLEMENTIRATI', 'asdads', 'asddsa', 'asdsad', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employer`
--
ALTER TABLE `employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
