-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 12, 2017 at 11:37 AM
-- Server version: 5.5.54-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `konektum_workit`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(9, 'PrirodneZnanosti'),
(10, 'TehnickeZnanosti'),
(11, 'BiomedicinaZdravstvo'),
(12, 'BiotehnickeZnanosti'),
(13, 'DrustveneZnanosti'),
(14, 'HumanistickeZnanosti'),
(15, 'UmjetnickoPodrucje'),
(16, 'InterdisciplinarnaPodrucjaZnanosti'),
(17, 'InterdisciplinarnaPodrucjaUmjetnosti');

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

CREATE TABLE `employer` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8mb4 NOT NULL,
  `last_name` varchar(35) CHARACTER SET utf8 NOT NULL,
  `company_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 NOT NULL,
  `city` varchar(20) CHARACTER SET utf8 NOT NULL,
  `fk_job` varchar(60) NOT NULL,
  `Active` varchar(3) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`id`, `first_name`, `last_name`, `company_name`, `email`, `password`, `city`, `fk_job`, `Active`) VALUES
(35, 'Karmen', 'Vlah Petohlep', 'L.U.A.N.A. D.O.O.', 'Luana.rijeka@gmail.com', 'a89b2afc414ed77d2c6ae02bb5efb1c3d0928d34', 'Rijeka', '1464437561.png', 'Yes'),
(37, 'poslodavac', 'poslodavac', 'Poslovanje', 'poslodavac@email.com', 'cd0f44378063b6d09bc50cf8d5ac20da11f0b24c', 'Rijeka', '1463658557.png', 'Yes'),
(43, 'Sebastian', 'SantraÄ', 'ALT Digital Agency (Alternative Developm', 'sebastian@altdev.hr', '1ef4bb728194a03e4233112fa47dd118ad94fc4c', 'PoreÄ', 'blankuser.jpg', 'Yes'),
(44, 'Sebastian', 'SantraÄ', 'ALT Digital Agency (Alternative Developm', 'sebastian.santrac@gmail.com', '17187f8e1e9edb0e9b1ca5d859a1e23b562cb84d', 'PoreÄ', '1463660434.png', 'Yes'),
(45, 'Sandra', '', 'Konektum', 'sandra.sabranovic@student.uniri.hr', '390487032f61f0c2d68aafa2e58b4fef0c4deddf', 'Rijeka', 'blankuser.jpg', 'Yes'),
(46, 'Arian', 'SantraÄ', 'Alternative Development d.o.o.', 'arian@altdev.hr', 'd052f85fa58fb0497ad4bb7f2d069dd486c4a9aa', 'PoreÄ-Rijeka', 'blankuser.jpg', 'Yes'),
(47, 'Milko', 'Lav', 'Poduzece d.o.o.', 'lol@gmail.com', '$2y$10$Lfa7Z8rKI/I3WIIHn2ep.egwo5rc/6bAcrrDMh2J0Eq/rrC8BCu.O', 'Osijek', '1489452692.png', 'Yes'),
(48, 'das', 'sdasd', 'fileme.org', 'svetlo_wrapcheboy@hotmail.com', 'c16bb7b60f35ab4e8b68329de6b9acb86732a7fb', 'virovitica', 'blankuser.jpg', 'Yes'),
(49, 'das', 'sdasd', 'fileme.org', 'marko.nwoh@gmail.com', 'adfb4b63bbc7f41d9c04f1430458a5f7a844c34e', 'virovitica', 'blankuser.jpg', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `name` varchar(351) CHARACTER SET utf8 NOT NULL,
  `description` varchar(4001) CHARACTER SET utf8 NOT NULL,
  `location` varchar(80) CHARACTER SET utf8 NOT NULL,
  `date` date NOT NULL,
  `file` varchar(60) CHARACTER SET utf8 NOT NULL,
  `fk_category` int(11) NOT NULL,
  `fk_employer` int(11) NOT NULL,
  `fk_student` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `name`, `description`, `location`, `date`, `file`, `fk_category`, `fk_employer`, `fk_student`) VALUES
(177, 'Maser na plaÅ¾i ', 'MasaÅ¾a na plaÅ¾i u klimatiziranoj kuÄici. ..Uvjeti odliÄni..PoÄetak rada 20.06. 2016.Kontakt 0958714990', 'KRK', '2016-05-28', '0', 11, 35, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `first_name` varchar(35) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `university` varchar(40) CHARACTER SET utf8 NOT NULL,
  `current_year` int(11) NOT NULL,
  `year_of_study` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 NOT NULL,
  `domicle` varchar(60) CHARACTER SET utf8 NOT NULL,
  `hobby` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `experience` varchar(4000) CHARACTER SET utf8 NOT NULL,
  `projects` varchar(4000) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `fk_employer` varchar(60) NOT NULL,
  `Active` varchar(3) NOT NULL DEFAULT 'No',
  `Taken` varchar(3) NOT NULL DEFAULT 'Da'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `first_name`, `last_name`, `university`, `current_year`, `year_of_study`, `email`, `password`, `domicle`, `hobby`, `experience`, `projects`, `fk_category`, `fk_employer`, `Active`, `Taken`) VALUES
(25, 'Paulina ', 'BerislaviÄ‡', '13', 1, '2015', 'paulinaberislavic@gmail.com', 'cbf5641412f2c69102794b4b67c3d17b5fc2e43b', 'Rijeka', 'Znanje engleskog ,njemackog jezika\r\nStudij smjera poslovnog upravljanja\r\n', 'Nema ', 'Uspijesnezene.net', 9, 'blankuser.jpg', 'Yes', 'Da'),
(28, 'Doris', 'Å estan', '15', 1, '2015.', 'dorissestan7@yahoo.com', 'b8e94746c3a55050f97f2fe1076605abbbee1dc4', 'Rijeka', 'VjeÅ¡tine: znanje engleskog jezika, komunikativnost, spremnost na timski kao i individualan rad..\r\nZavrÅ¡ena srednja Medicinska Å¡kola u Rijeci, smjer- TehniÄar nutricionist\r\nPutem prakse u srednjoj Å¡koli steÄene su vjeÅ¡tine rada u ljekarni te kuhinjama raznih institucija\r\n\r\nStudijski smjer- Poslovno upravljanje\r\nInteresi povezani sa navedenim obrazovanjem.', 'Nema', 'Rad na web stranici uspjesnezene.com', 9, '1463428462.png', 'Yes', 'Da'),
(30, 'Mario', 'LonÄar', '7', 3, '2012', 'mario.loncar@uniri.hr', '7c222fb2927d828af22f592134e8932480637c0d', 'Rijeka', 'Android development, Java', '2.5 godine izrada Android aplikacija', 'Find my ATM, RiBus, STEMI', 9, 'blankuser.jpg', 'Yes', 'Da'),
(32, 'Jasmin', 'Abou Aldan', '7', 4, '2016', 'jasmin.abou.aldan@uniri.hr', '7c222fb2927d828af22f592134e8932480637c0d', 'Rijeka', 'iOS development', '3 godine kao iOS developer', 'RiBus, STEMI App', 9, 'blankuser.jpg', 'Yes', 'Ne'),
(35, 'Samuel', 'SavanoviÄ‡', '7', 4, '2012', 'samuel.savanovic@gmail.com', 'fb6c4fba8cf15d08422ca0a0106e538fc4a9df56', 'Rijeka', 'C++, C, php, js, java, C#, linux', 'Back end developer za konektum.com', 'konektum.com i neki manji utility alati u C-u.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(36, 'Ivana', 'ÄorÄ‘eviÄ‡', '7', 4, '2012', 'ivana.dordevic@student.uniri.hr', '0601fab640f0c83621278e24a1d0d73ff976d5cb', 'Rijeka', 'GrafiÄki dizajn, web dizajn', 'GrafiÄki dizajner i front end developer za konektum.com', 'konektum.com', 9, '1463674842.jpg', 'Yes', 'Da'),
(37, 'Otavio', 'AniÄiÄ‡', '7', 4, '2012', 'otavio.anicic@gmail.com', '416b5949243927cdd4a35d753763592eb98cf3e7', 'Rijeka', 'vjestine vezane uz programiranje ', 'nema', 'nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(38, 'Ana', 'SabliÄ‡', '12', 3, '2013', 'ana.sablic@hotmail.com', 'ead32a7b29d2aae8174cde503c3d1bb5da21f2b7', 'Rijeka', 'Rad s djecom rane i predÅ¡kolske dobi', 'Dadilja (dva puta tjedno)\r\nVolontiranje: u Äitaonoci na Zametu s djecom mlaÄ‘om od 3. godine', 'nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(39, 'Enzo', 'Licul', '11', 4, '2012', 'enzo.licul@yahoo.com', '82b17c9cac52dbdc9209dd90ea1c244b2cb7e662', 'Rijeka', 'Php, c/c++', 'Aaaaaaa', 'Bbbbbbbbb', 9, 'blankuser.jpg', 'Yes', 'Da'),
(40, 'Izabel', 'SuÅ¡anj', '7', 4, '2012.', 'izabelsusanj@gmail.com', 'a615fc36c20932d76415b9c449637ba6d12bb919', 'Rijeka', '- HTML5, CSS3, PHP, Bootstrap\r\n- C++\r\n- Python\r\n- Arduino\r\n- Java', '/', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(41, 'Sandra', 'SabranoviÄ‡', '7', 4, '2012', 'sandra.sabranovic@gmail.com', '390487032f61f0c2d68aafa2e58b4fef0c4deddf', 'Rijeka', '-', '-', '-', 9, '1463426567.png', 'Yes', 'Da'),
(42, 'Filip', 'Bandalo', '7', 4, '2012', 'filip.bandalo@student.uniri.hr', '628bb4f4bd9424a122c84e7cd7d29254b10e13ed', 'Rijeka', '', '', '', 9, 'blankuser.jpg', 'Yes', 'Da'),
(43, 'Mirjam', 'SabliÄ‡', '7', 2, '2014.', 'mirjam.sablic@hotmail.com', '36b5f97ec77a353df36d84f3c09fe48c0606755e', 'RIJEKA', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(44, 'Niko', 'Konjuh', '7', 2, '2015', 'niko.konjuh@hotmail.com', '7bb09414511180863ea4d82876c5ae7271b5d393', 'Rijeka', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(45, 'Filip', 'DidoviÄ‡', '7', 0, '2014', 'filip.didovic7@gmail.com', '3315bcebb0e1a8e63843c413f55ca12dcea92fd7', 'Rijeka', 'Komunikacija s ljudima, poznavanje engleskog, talijanskog i njemaÄkog jezika, poslovno pisanje', 'Dvije godine rada na recepciji turistiÄke agencije, radnik u skladiÅ¡u firme za transport dobra, radnik na bazenu, praksa za vrijeme trajanja srednje Å¡kole u obliku recepcionara, 2 mjeseca rada kao agent u sluÅ¾bi za korisnike.', '.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(46, 'Antonela', 'Troha', '7', 0, '2014', 'antonellatroha@gmail.com', '4eefd9162d09ffd8afaa522412311f0cdc814b66', 'Rijeka', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(47, 'Josipa', 'NovakoviÄ‡', '4', 2, '2014', 'josipa.novakovic18@gmail.com', 'bb17396237bec6aec4aa7fb58177cbe2a0104aba', 'Rijeka', 'PruÅ¾anje prve pomoÄ‡i ', '.', '.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(48, 'Dorjan', 'VukeliÄ‡', '11', 3, '2013', 'dorjanvuk@gmail.com', '614f4112f70405b5e3fb975949df71a9a602f17e', 'Rijeka', 'Dobro poznavanje AutoCAD programa i rad sa sliÄnim alatima.\r\nUpoznat sa osnovnim funkcijama Pythona, C i microPascal jezika. Imam iskustvo rada u Proteus-u te viÅ¡egodiÅ¡nje iskustvo koriÅ¡tenja i aktivnog rada u Matlab i Mathcad alatima (za potrebe studija).\r\nImam iskustvo rada sa svim MS Office alatima te opÄ‡enito jako dobro poznavanje Windows OS-a te rada u njemu.\r\nZanima me i volim sve Å¡to ima veze sa strukom i Å¾eljan sam stjecanja novih znanja.', 'Za vrijeme srednje Å¡kole i fakulteta:\r\n6 mjeseca rada u CTS d.o.o. u sklopu struÄne prakse\r\n\r\nPosao van struke:\r\n2 godine rada u korisniÄkoj sluÅ¾bi Hrvatskog Telekoma', 'Trenutno nema projekata na kojima radim.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(49, 'Eugen', 'Badanjak', '13', 3, '2013.', 'eugen.badanjak94@gmail.com', 'e2e310062ce7d7be616a3bf6644ee9cc51ec35f3', 'Rijeka', 'Osnove elektrotehnike\r\nâ–ª Programiranje u jeziku C / Algoritmi i strukture podataka\r\nâ–ª Internet komunikacija\r\nâ–ª Teorija signala\r\nâ–ª Programiranje u Javi\r\nâ–ª Programiranje u SQL\r\nâ–ª RAD CASE alati\r\nâ–ª Projektiranje sustava\r\nâ–ª HTML,CSS,Javascript,Php\r\nâ–ª GIS (Geografski informacijski sustavi)\r\nâ–ª Osnove PLC programiranja / Automatizacija', 'Nista.', 'Nista.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(50, 'Viktor', 'BleÄiÄ‡', '9', 1, '2015.', 'cicble@gmail.com', '14c10f648eae270a684100a2b2372b4dd6b85be6', 'Rijeka', '.', 'Agent sluzbe za korisnike u Hrvatski Telekom', '.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(51, 'Ivona', 'VincetiÄ‡', '12', 3, '2013.', 'ivy.vincetic@gmail.com', '55e7a39024695dffdb580db2f0ea4001a05f6d99', 'Rijeka', 'komunikativnost', 'djeÄja igraonica, promocije, slaganje robe', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(52, 'Toni', 'BrniÄ‡', '4', 3, '2014', 'toni_cres@hotmail.com', '08508d523206b293fad05a793b1116e10a5113b7', 'Rijeka', 'VjeÄ‘tine vezane za smjer sanitarno inÅ¾enjerstvo.', 'Rad u kiosku.', 'Nemam.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(53, 'Kristijan', 'Bosilj', '4', 3, '2013', 'kristijan.bosilj1994@gmail.com', '0e5e74b45e1481a5291e4282431724b77cc365d8', 'Rijeka', 'Vrlo se dobro snalazim u laboratoriju.', 'Nemam ( osim konobarenja )', 'ZavrÅ¡ni rad: \"Antibakterijski uÄinak propolisa na odabrane patogene bakterije\"\r\n', 9, 'blankuser.jpg', 'Yes', 'Da'),
(54, 'Lalita', 'BariÅ¡iÄ‡', '4', 3, '2013./2014.', 'barisic.lalita@gmail.com', '95a3fbf76f66cd61e3f6b7d83ead5e6e98db1301', 'Rijeka', 'Znanja iz podruÄja biomedicine, polja javnog zdravstva i zdravstvene zaÅ¡tite. \r\nSteÄeno zanimanje farmaceutskog tehniÄara.', 'Rad u ljekarni, sezonski posao ÄiÅ¡Ä‡enja jahti i hostesiranje.', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(55, 'Enea', 'SkraÄiÄ‡', '2', 3, '2013./2014.', 'enea1904@hotmail.com', 'f253870682504316a7e456b2d1e70201aa7aff2b', 'Rijeka', 'Komunikativnost, otvorenost za iskustva, rad s ljudima, sportsko iskustvo, volontiranje', 'Animator, spasioc, konobar, plivaÄki sudac, plivaÄki trener', 'Rijeka psihologije', 9, 'blankuser.jpg', 'Yes', 'Da'),
(56, 'Fabian', 'VasiÄ‡', '3', 3, '2012.', 'fabianvasic.vaso@gmail.com', '9a0ef19a75b65615e78a14a806dd0c5cdeff1f19', 'Cres', '2D crtanje u autocadu', 'nema', 'nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(57, 'Ilaria ', 'Gazilj', '2', 4, '2012.', 'ilaria.gazilj@gmail.com', 'cbf64488316a0c689b83267cf1e63063cc226d6c', 'Rijeka', 'verbalne,neverbalne komunikacijske vjeÅ¡tine steÄene tijekom visokog obrazovanja\r\nsposobnost aktivnog sluÅ¡anja i empatiÄnog razumijevanja sudionike u odgojno-obrazovnim procesima\r\nsposobnost timskog rada, kooperativne suradnje\r\nfleksibilnost i prilagodljivost neoÄekivanim situacijama\r\nsposobnost za rad u odgojno-obrazovnim organizacijama, poput Å¡kole, doma i sliÄno\r\nsposobnost pruÅ¾anja Å¡kolskog savjetodavnog rada uÄenicima\r\nsposobnost osmiÅ¡ljanja Å¡kolskog kurikuluma, plana i programa rada Å¡kole\r\nsposobnost voÄ‘enja Å¡kolske pedagoÅ¡ke dokumentacije', 'ProdavaÄ, Recepcionist', 'Projekti i istraÅ¾ivanja u podruÄju emocionalne pedagogije, pedagogije rada', 9, 'blankuser.jpg', 'Yes', 'Da'),
(58, 'Hana ', 'GaÄal', '2', 4, '2011.', 'hana.gacal@gmail.com', 'b568a8e2e5f0bdf7db8ad1b9d02bd3c8901013bb', 'Rijeka / ÄŒakovec', 'KliniÄka psihologija, zdravstvena psihologija, neuropsihologija, istraÅ¾ivanja...', '-', 'Å½enska agresija', 9, 'blankuser.jpg', 'Yes', 'Da'),
(59, 'Ana', 'NikÅ¡iÄ‡', '2', 1, '2015', 'aniksic@ffri.hr', '987f25f3795a7a3b7cfd0821dc2eef20e9e1ffbe', 'Rijeka', 'Rad u SPSS-u, osnove kliniÄke procjene, znanja statistike, psihometrije i metodologije', 'Volonterski i sezonski rad', 'U pripremi', 9, 'blankuser.jpg', 'Yes', 'Da'),
(60, 'Doris', '', '2', 0, '2012/2013', 'doris.bencic@gmail.com', '7cc95ec619057140edc53d88c5160591e30032fe', 'Pula', 'organizacijska psihologija, kliniÄka psihologija', 'nema', 'Psihozij 2016. - Neki prediktori Å¾enske agresije', 9, 'blankuser.jpg', 'Yes', 'Da'),
(61, 'Irena', 'Superina-Gudelj', '13', 2, '2013.', 'irenasg@yahoo.com', '2b6c36de673f8fc208996ceb2c7a2ddeec6ca500', 'Rijeka', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(62, 'Milan', 'PetroviÄ‡', '7', 3, '2013', 'dr.milojko@gmail.com', '35e3daac86ceaac8d7f356a46ac1185748cbb252', 'Rijeka', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(63, 'Veronika', 'Zajec', '1', 0, 'Prva', 'Veronikazajec.vz@gmail.com', 'b37142acea9daf637fdc271215c3c16cb62e75f2', 'Rijeka', 'Hostesiranje ,konobarenje , drogerije ,papirologija ', 'Hostesiranje ,konobarenje ', 'Nema ', 9, 'blankuser.jpg', 'Yes', 'Da'),
(64, 'Veronika', 'Zajec', '1', 0, 'Prva', 'Zajec.veronika@gmail.com', 'd8c46b4d7f39cdfc3dda8429807104f684035460', 'Rijeka', 'Hostesiranje , papirologija , kozmetika', 'Hostesiranje , konobarenje', 'Nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(65, 'Ella', 'Palfi', '1', 2, '2014', 'ella.palfi@gmail.com', '8834d6a03c0b728fd417007b9148e1786003cec9', 'Rijeka', '..', 'Nemam', '.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(66, 'Glorija', 'PeraniÄ‡', '12', 3, '1', 'glorija.peranic@gmail.com', 'a2caf8f82ecd9f5cbeaf8fa910fb670b1f8036dc', 'Rijeka', 'Komunikacijske vjeÅ¡tine, Äuvanje djece, promocije proizvoda', 'Hostesa,  rad u djeÄjoj igraonici, predstavnica UNICEF-ove kampanje \"ÄŒuvari djetinjstva\", rad u ÄŒudotvornici-radionice s djecom u sklopu Povijesnog i pomorskog muzeja grada Rijeke', 'MeÄ‘unarodni projekt \"United to end violence against women\"', 9, 'blankuser.jpg', 'Yes', 'Da'),
(67, 'KreÅ¡imir', 'JarediÄ‡', '3', 0, '2015.', 'kresimir.jaredic@uniri.hr', '9e12b2d97df02c509d6d9285839151a71f790f6a', 'Rijeka', 'Izvrsno znanje programa Autocada, Mathcada, Ki experta i drugih, te su mi interesi energetska uÄinkovitost objekata, kao i izrada projekata (nacrta)  za obiteljske kuÄ‡e ili druge objekte. ', 'Izrada energetskih certifikata, izrada elaborata u legalizaciji, izrada projektne dokumentacije za kuÄ‡e, rad na terenu', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(68, 'Petra', 'SriÄ‡a', '1', 1, '1', 'petra.srica@hotmail.com', '591aabd3db68f53ce6eb2b57bb5ea08d1e7c5547', 'Rijeka', 'Zanima me rad u administraciji, pomaganje odvjetniku / javnom biljeÅ¾niku', 'Do sada joÅ¡ nisam radila u struci, ali mimo toga radila sam kao promotorica po raznim trgovaÄkim centrima, u DM-u, u radu s djecom..', 'Nemam napisane znanstvene radove.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(69, 'sara', 'kalajdÅ¾iÄ‡', '1', 1, '2015/2016', 'sara.kalaj@gmail.com', 'cd35ad340cf13fe840c8475d39cda34d2331e7d9', 'rijeka', 'Komunikativna, marljiva, spremna na timski rad, organizirana, aktivno se sluzim internetom, govorim engleski jezik odlicno (pisanje, citanje i govor)', '9 mjeseci sam radila kao prodavac/blagajnik u SPAR trgovini (SPAR HRVATSKA d.o.o. - filijala Bijenicka 5, 10000 Zagreb)', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(70, 'Anabella', 'MalbaÅ¡a', '14', 4, '2012', 'mistybellica@gmail.com', 'afd5dd522cf5ded4790c876d6e40c5fe7abffdc8', 'Rijeka', 'RaÄunovodstvo, knjigovodstvo', '5 godina na blagajni, info pultu, raÄunovodstvu.', 'Ne', 9, '1464689916.JPG', 'Yes', 'Da'),
(71, 'Ivana', 'Kusturin', '14', 4, '2012.', 'Ivana.kusturin@hotmail.com', '0e12ff18f7416b98a6f53e2a442a91b27eb754f4', 'Rijeka', 'VjeÅ¡tine steÄene u prodaji i promotivnim aktivnostima, izraÅ¾ena komunikativnost, pristupaÄnost, organiziranost, odgovornost. Interesi za rad u podruÄju marketinga, financija.', 'ProdavaÄ - CCC Rijeka (2015.-danas)\r\nPromotor - Fini oglasi/Aretis (2013.-danas)\r\nTeleprodavaÄ - Hrvatski telekom (2013.-2014.)', 'Projekt EU-a: Comenius (4.razdred srednje Å¡kole)\r\nSudjelovanje u projektu: Career Booster 2016.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(72, 'Ivana', 'Kusturin', '14', 4, '2012.', 'ekonomistica.IK@gmail.com', '0e12ff18f7416b98a6f53e2a442a91b27eb754f4', 'Rijeka', 'VjeÅ¡tine steÄene u prodaji i promotivnim aktivnostima, izraÅ¾ena komunikativnost, pristupaÄnost, organiziranost, odgovornost. Interesi za rad u podruÄju marketinga, financija.', 'ProdavaÄ - CCC Rijeka (2015.-danas)\r\nPromotor - Fini oglasi/Aretis (2013.-danas)\r\nTeleprodavaÄ - Hrvatski telekom (2013.-2014.)', 'Projekt EU-a: Comenius (4.razdred srednje Å¡kole)\r\nSudjelovanje u projektu: Career Booster 2016.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(73, 'Doris', 'BoÅ¾iÄeviÄ‡', '14', 3, '2010./2011.', 'krumpirko03@gmail.com', '609bb9de37c3711d459ed64839aaa6bbcc1b8ec3', 'Veli LoÅ¡inj', 'Rad u skupinama, rad s ljudima, anketiranje', 'Anketiranje, rad u uredu', 'nemam ih', 9, 'blankuser.jpg', 'Yes', 'Da'),
(74, '', '', '14', 0, '', '', '1c13b2f1cf91c1dd15cb573c603551146ea6b423', '', '', '', '', 9, 'blankuser.jpg', 'Yes', 'Ne'),
(75, 'Marko', 'ZoviÄ‡', '14', 4, '2012', 'shaququ@gmail.com', '36a76b861151b69e0bd3b992e18ccb89f965a976', 'PoreÄ', 'ekonomija', 'nema', 'nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(76, 'Nensi', 'SlavujeviÄ‡', '14', 2016, '2013', 'nensi061@gmail.com', '60636bf34e5edb95c606bd7fa09c16667bccec40', 'Rijeka', 'Engleski, Å¡panjolski, talijanski\r\nDobro vladanje alatima Microsoft Office (Word, Excel,PowerPoint, Publisher)', '01/03/2016-01/04/2016 StruÄna praksa - Allianz osiguranje, Rijeka (Hrvatska)\r\n01/02/2015â€“28/02/2015 Administrativni referent - ZajedniÄki odvjetniÄki ured Sanja Kamenar Milutin i Irena Klepac MustaÄ‡, Rijeka (Hrvatska)\r\n01/07/2014â€“01/09/2014 Postavljanje deklaracija na proizvod - Plodine d.d., Bakar (Hrvatska)\r\n01/05/2014â€“01/07/2014 Trgovac - DM-drogerie market d.o.o., Rijeka (Hrvatska)\r\n01/06/2013â€“01/09/2013 Promotor - Libra d.o.o., Rijeka (Hrvatska)\r\n01/06/2012â€“01/09/2012 Promotor - Libra d.o.o., Rijeka (Hrvatska)', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(77, 'Maja', 'Belobaba', '7', 4, '2012', 'maja.belobaba@student.uniri.com', '216ca80b52ba96ce4e1f9a69ea03b3dcc8694478', 'Rijeka', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(78, 'Maja', 'Belobaba', '7', 4, '2012', 'maja.belobaba@student.uniri.hr', '216ca80b52ba96ce4e1f9a69ea03b3dcc8694478', 'Rijeka', '-', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(79, '', '', '14', 0, '', '', '$2y$10$EnDmqd3Q7bq4ZtLde2nnYuV8JHfe5gc5ef.aa0ldQ0U3fiqTRiRmm', '', '', '', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(80, 'Neven', 'Krivak', '13', 4, '2011.', 'neven.krivak@hotmail.com', '65496a2f0a1b60c0c89a71fc353100d53e3b0838', 'Rijeka', 'ZavrÅ¡eni preddiplomski studij financije i raÄunovodstvo na Ekonomskom fakultetu u Rijeci, interesira me poslovanje, meÄ‘unarodna trgovina, tehnoloÅ¡ke inovacije. Trenutno sam na diplomskom studiju Ekonomskog fakulteta u Rijeci, smjer poduzetniÅ¡tvo.', 'Portir, prodavaÄ, konobar, Älan izbornog povjerenstva i biraÄkog odbora, roÅ¡tilj majstor.', '-', 9, '1465145284.jpg', 'Yes', 'Da'),
(81, 'Maja', 'BeniÄ‡', '14', 5, '2011.', 'maja.ben1c92@gmail.com', '4b1d4c465fc75fc0d2f0be38a30b649cc10f2de1', 'Rijeka', 'Engleski jezik (A1,A2), MS Office, dobre komunikacijske vjeÅ¡tine i prilagodba na nepredviÄ‘ene situacije (kultura, jezik), ', '15 travnja 2015â€“danas\r\nProdavaÄica\r\nProjekt Zapad d.o.o., Rijeka (Hrvatska)\r\nProdaja sladoleda, komunikacija s kupcima, rad na blagajni, obraÄun dnevnog utrÅ¡ka.\r\n\r\n01 lipnja 2014/2015â€“01 rujna 2014/2015\r\nSezonski posao - ProdavaÄica\r\nAsteriks d.o.o., Krk, otok Krk (Hrvatska)\r\nProdaja sladoleda, komunikacija s kupcima, rad na fiskalnoj blagajni, sastavljanje obraÄuna.\r\n\r\n01 lipnja 2013â€“01 rujna 2013\r\nSezonski posao - pomoÄ‡ni radnik u kuhinji\r\nHoteli Haludovo Malinska d.d., Malinska, otok Krk (Hrvatska)\r\nPomoÄ‡ni poslovi u kuhinji (ÄiÅ¡Ä‡enje, pranje, pomoÄ‡ pri izradi jela)', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(82, 'Marko', 'Å trljiÄ‡', '14', 4, '2015', 'markostrljic@gmail.com', '21aedc15f2a675acf3f04a11d0af570027f14cc0', 'Rijeka', 'Dobro sluÅ¾enje microsoft office paketom. VozaÄka dozvola B kategorije. Izvrsno sluÅ¾enje engleskim jezikom. ', 'ProdavaÄ autodijelova u tvrtci Riven d.o.o. od 2013 - 2015. god.', 'Nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(83, 'Tena', 'Gelemanovic', '14', 0, '2015', 'tency1212@gmail.com', '61ed220603c3ccaa9b275e3f2374970daf098ba2', 'Crikvenica', '-', 'Start up projekt', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(84, 'Andrija', 'MiniÄ‡', '3', 3, '2010', 'andrija92minic@gmail.com', '306aeb53a40f06584bbbbdf97837dd34ef12905b', 'Pula', 'AutoCAD: Crtanje u ravnini, 3D modeliranje\r\nMicrosoft Office: Word, Excel, Power Point', 'Nema', 'Nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(85, 'Anamarija', 'VinkoviÄ‡', '2', 1, '2015', 'anamarija_vinkovic@hotmail.com', '8363f201797451d538a88845d6363aa3c856b256', 'Rijeka', '- Komunikacijske, prezentacijske i poduzetniÄke vjeÅ¡tine\r\n-  Kreativnost (sposobnost crtanja i izraÄ‘ivanja raznih materijala)\r\n- Spremnost za suradnju i timski rad\r\n- Interes u radu u sustavu obrazovanja odraslih te rad u udrugama civilnog druÅ¡tva\r\n- Interes za rad sa srednjoÅ¡kolskim uzrastom i mladima', '02/2016â€“danas -Organizator\r\n- Udruga Delta, Rijeka (Hrvatska)\r\nâ–ªOrganiziranje i kreiranje projekta \"Val ideja\"\r\nâ–ªSuradnja i timski rad s Älanovima tima organizacije te Älanovime Udruge Delta\r\n\r\n11/2015â€“danas - Volonter\r\n- Udruga Delta, BlaÅ¾a PoliÄ‡a 2/IV, 51000 Rijeka (Hrvatska) http://www.udruga-delta.hr/\r\nâ–ªSudjelovanje u projektu \"Akcija za 5!\" - uloga koordinatora uÄenika srednjih Å¡kola na podruÄju grada Rijeke i priprema materijala za projekt\r\nâ–ªOsmiÅ¡ljavanje i organizacija konferencije za uÄenike srednjoÅ¡kolske dobi - \"MiniKonfa\" : uloga kreatora, organizatora, voditelja i izlagaÄa\r\nâ–ªSuradnja i timski rad s Älanovima udruge te studentima i volonterima\r\n\r\n27/11/2013â€“28/05/2014 - Volonter\r\n- Dom za odgoj djece i mladeÅ¾i Rijeka, Vukovarska 49, 51000 Rijeka (Hrvatska) http://dzo-rijeka.hr/o-nama\r\nâ–ªRad s osnovnoÅ¡kolskom i srednjoÅ¡kolskom djecom s teÅ¡koÄ‡ama u uÄenju i ponaÅ¡anju - u sklopu PSP-a (ProduÅ¾enog struÄnog postupka u sklopu osnovne Å¡kole) i PIT-a (polu-institucionaliziranom trenutnom boravku djece u Domu za odgoj djece i mladeÅ¾i)\r\nâ–ªOdgovornosti u sklopu PSP-a u Osnovnoj Å¡koli Gornja VeÅ¾ica:\r\n- Pisanje zadaÄ‡e i uÄenje s djecom s teÅ¡koÄ‡ama u uÄenju i ponaÅ¡anju (lakÅ¡i oblici); Kreativne - likovne radionice s djecom; Sportske aktivnosti\r\n- Suradnja sa struÄnim suradnicima Osnovne Å¡kole i Doma za odgoj djece i mladÅ¾i (socijalnim radnicima, socijalnim pedagozima, psiholozima i pedagozima)\r\nâ–ªOdgovornosti u sklopu PIT-a u Domu za odgoj djece i maldeÅ¾i Rijeka:\r\n- Pisanje zadaÄ‡e i uÄenje s djecom s teÅ¡koÄ‡ama u uÄenju i pouÄavanju (teÅ¾i oblici); Kreativne - likovne radionice; Suradnja sa struÄnim suradnicima; Pisanje dnevnika volontiranja', '02/2016â€“danas - Organizator\r\n- Udruga Delta, Rijeka (Hrvatska)\r\nâ–ªOrganiziranje i kreiranje projekta \"Val ideja\"\r\nâ–ªSuradnja i timski rad s Älanovima tima organizacije te Älanovime Udruge Delta\r\n\r\n\r\n2015 - Koordinator uÄenika na projektu \"Akcija za 5!\" i organizator konferencije za srednjoÅ¡kolske uÄenike \"MiniKonfa\"\r\n', 9, 'blankuser.jpg', 'Yes', 'Da'),
(86, 'Dora', 'MilakoviÄ‡', '4', 2, '2014./2015.', 'milakovicdora@gmail.com', '2071b8d47795d86b7c5c5c596beb517dba8b19b0', 'Zagreb', 'Redovito sudjelujem u aktivnostima Cromsica, organiziram aktivnosti za studente na razmjeni, Älanica sam studentskih sekcija, radim na znanstvenom radu', 'Praksa u bolnici', 'Radim na znanstvenom radu Teratomi i teratokarcinomi jajnika i testisa', 9, 'blankuser.jpg', 'Yes', 'Da'),
(87, 'Danijel', 'Pocrnja', '7', 3, '2013', 'danijel.pocrnja@gmail.com', 'f8dbc67d7dd436f231b002c3df6f276781508ca4', 'ÄŒazma', 'Java, C, C++, Web design(html, css, php), SQL', 'U struci nemam radnog iskustva, spreman sam uÄiti brzo i kvalitetno', 'Web sjediÅ¡te(dinamiÄko sa Äitanje baze podataka i unososm u bazu), Java(blagajna za informatiÄki duÄ‡an)', 9, 'blankuser.jpg', 'Yes', 'Da'),
(88, 'Anja', 'NovakoviÄ‡', '6', 4, '2012', 'anja.novakovic2012@gmail.com', '43c34fdc5c6425126df9324dc841afb89bdb7129', 'Osijek', 'yyy', 'yyy', 'yyy', 9, 'blankuser.jpg', 'Yes', 'Da'),
(89, 'Ivan', 'PeriÅ¡a', '13', 3, '2012', 'ivan.perisa8@gmail.com', 'cc4723995ce819915e734147a77850427a9e95f9', 'Rijeka', 'a', 'a', 'a', 9, 'blankuser.jpg', 'Yes', 'Da'),
(90, 'Ana ', 'Å varc', '8', 3, '2013.', 'svarc.ana@gmail.com', 'f6d6100dbccffac62e0542f0b6c0e14727956445', 'Zagreb', 'paralelno studiram ekonomiju, zanima me podruÄje financija,procjene rizika te informatiÄki menadÅ¾ment', 'davanje instrukcija iz matematike i fizike za osnovnu i srednju Å¡kolu i pripreme za maturu', '.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(91, 'Gloria', 'Hudolin', '10', 4, '2011.', 'gloria.hudolin@gmail.com', '635ad49e4cfa8d1d418f0ea88296e74bbc0f1ff9', 'Rijeka', 'Aktivni govor i pisanje engleskog jezika, te pasivnog talijanskog.\r\nZainteresiranost za pronalazak struÄne prakse ili volonterstvo u podruÄju svih pravnih grana, posebno obiteljskog, graÄ‘anskog, trgovaÄkog i kaznenog prava.', 'Bez struÄnog iskustva', 'Seminarski rad iz podruÄja radnog prava', 9, 'blankuser.jpg', 'Yes', 'Da'),
(92, '', '', '5', 0, '', '', '14385952b1ccf0c973a11a3af3100c152f83b2d6', 'Zagreb', '.', '', '', 9, 'blankuser.jpg', 'Yes', 'Ne'),
(93, 'AnÄ‘ela ', 'JosipoviÄ‡', '2', 3, '2013.', 'josipovic.andela@gmail.com', 'fc9240e76ef3d085760793049c9a2588d42b5f53', 'Zagreb', 'Psihologija', '/', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(94, 'Juraj', 'PerkoviÄ‡', '2', 0, '2012.', 'jura.perkovic1@gmail.com', 'a190351968c2b3565c61fb6f1efe260ecc55aaa2', 'Zagreb', 'Kvantitativna i Kvalitativna istraÅ¾ivanja. Organizacijske vjeÅ¡tine. Microsoft office paket. \r\nInteresi: Marketing, IstraÅ¾ivanje trÅ¾iÅ¡ta, Brendiranje.', 'Do diplomskog studija nisam imao vremena raditi uz studij (osim noÄ‡nih studentskih poslova). Sada na diplomskom sam slaÅ¾em svoj raspored te se mogu prilagoditi bilo kakvom radnom vremenu.\r\nPotpredsjednik Udruge za promicanje alternativne kulture.\r\n', 'Projekti vezani za organiziranje raznoraznish dogaÄ‘aja vezanih za Udrugu za promicanje alternativne kulture.\r\nIstraÅ¾ivanje politiÄke homogenosti unutar stranke \"Å½ivi zid\".\r\nIstraÅ¾ivanje medikalizacije (provedeno na studentima SveuÄiliÅ¡ta u Zagrebu). \r\nVolontirao na: Sjednici Hrvatskog SocioloÅ¡kog DruÅ¡tva; Outlook festivalu; Dimensions festivalu.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(95, 'Darija', 'SuÅ¡ac', '13', 4, '2012', 'susac.darija@gmail.com', 'cddf86c51b7d7cf75b968898b81cdfdd22612803', 'Zagreb', 'GEODETSKI FAKULTET, SVEUÄŒILIÅ TE U ZAGREBU\n\n- Ovaj studijski program osposobljava za obavljanje sljedeÄ‡ih poslova: geodetski mjernik, GIS-suradnik,\nkatastarski mjernik, fotogrametar suradnik, kartograf suradnik i hidrograf suradnik, te osposobljava za\npraÄ‡enje tehnoloÅ¡kih promjena u geodeziji i geoinformatici.\n\n-PohaÄ‘anje besplatnih online teÄajeva (Python, HTML, CSS)', '- Kolegij \"StruÄna praksa izvan fakulteta\"\r\nTijekom akademske godine 2013/2014 upisala sam izborni kolegij \"StruÄna praksa izvan\r\nfakulteta\". Tijekom dvoljetne prakse imala sam moguÄ‡nost odraÄ‘ivanja jednostavnih\r\ngeodetskih zadaÄ‡a (provoÄ‘enje poligonskog vlaka, iskolÄenje, koriÅ¡tenje GPS ureÄ‘aja,...),\r\nupoznavanje sa programom OpenJUMP te moguÄ‡nost uvida u timski rad unutar poslovne\r\nsredine.Â¸\r\n\r\n- Studentski demonstrator\r\nBila sam studentski demonstrator na preddiplomskom izbornom kolegiju \"Uvod u menadÅ¾ment\"\r\n(zimski semestar 2013/2014).', 'NiÅ¡ta.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(96, 'Petra', 'Pervan', '2', 4, '2011.', 'petra.pervan92@gmail.com', '942145a550dabe50bb50af5bd89ced92bcb542d5', 'Zagreb', 'Organizacija i provoÄ‘enje edukacija na temu grupnih procesa, komunikacije i voÄ‘enja, rad s djecom, mladima i odraslima, savjetovanje', 'Volontiranje na savjetodavnoj liniji Hrabrog telefona te osmiÅ¡ljavanje i odrÅ¾avanje radionica za prvi i drugi razred osnovne Å¡kole u Centru Sirius, Predavanje \"Creative leader, motivated group\" u organizaciji Kolping Europe, predavanje \"Kako sluÅ¡ati bliÅ¾njega svoga\" u organizaciji Nacionalnog vijeÄ‡a FRAME, Äuvanje djeÄaka s ADHD-om u privatnom angaÅ¾manu, voÄ‘enje programa \"Mladi za mlade\" u Uredu HBK za mlade od 2011. do 2015. godine', 'Predavanja navedena u radnom iskustvu', 9, 'blankuser.jpg', 'Yes', 'Da'),
(97, 'Marko', 'TomiÄ‡', '2', 6, '2010', 'markotom7@gmail.com', 'd37d3660285916aff096afd3ccc73c1530581ec1', 'Zagreb', 'PrevoÄ‘enje na engleski i ruski i sa istih jezika, poduÄavanje jezika i sastavljanje lekcija i predavanja, lektoriranje tekstova', 'honorarno prevoÄ‘enje i lektoriranje engleskih i ruskih tekstova (beletristika i struÄni tekstovi), poduÄavanje poslovnog engleskog jezika, instrukcije', 'objava prevedene kratke priÄe V. Pelevina u Äasopisu studenata komparativne knjiÅ¾evnosti \"Ka/Os\" pri FFZG, objava prevedene kratke priÄe u Antologiji sovjetske i post-sovjetske kratke proze \"Potemkinovo selo\", uloga lektora u projektu organizacije Simpozija povijesniÄara umjetnosti na Filozofskom fakultetu u Zagrebu 2014.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(98, 'Viktorija', 'PlatuÅ¾iÄ‡', '14', 4, '1.', 'viktorija.platuzic.zg@gmail.com', 'a837925a09706115b62f383eed01cbe6cfcb3523', 'Zagreb', 'Ekonomija, Financije', '/', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(99, 'Jelena', 'PeriÅ¡a', '3', 2, '2014.', 'jelena_perisa@hotmail.com', '49293791bd8f351a9094fa63a58876ad138928c9', 'Split', 'Sportovi, izvannastavne aktivnosti, poslovi, Å¡kole stranih jezika su osim vremena provedenog sudjelovajuÄ‡i, od mene takoÄ‘er zahtjevali i dobre organizacijske vjeÅ¡tine. Nikada zanemarujuÄ‡i Å¡kolu, uspjela sam dosta vremena odvojiti za aktivnosti koje su od mene traÅ¾ile i pripremu . Danas , baveÄ‡i se volonterskim radom ( u crkvi sv. Marije PomoÄ‡nice (KneÅ¾ija) predajem matematiku za srednju Å¡kolu) te studirajuÄ‡i na GraÄ‘evinskom fakultetu â€‹shvaÄ‡am koliko vaÅ¾nu ulogu su takve aktivnosti imale za mene. Sposobna sam procjeniti koliko mi je vremenski potrebno svladati odreÄ‘eno gradivo te u skladu s tim organiziram slobodno vrijeme ( odlaskom u kino, kazaliÅ¡te, Å¡etnje).SudjelovajuÄ‡i na raznim projektima, od mene se zahtjevala velika odgovornost i organiziranost. RukovodeÄ‡i te pomagajuÄ‡i drugima, stekla sam iskustvo u timskom radu te relativno brz pronalazak rjeÅ¡enja brojnih kompleksnih i nenadanih situacija. UzimajuÄ‡i u obzir Å¾elje te prijedloge mnogih, uspjeli smo doÄ‡i do kompromisa koji odgovara svima.', '(7.-8.2012.) prodaja suvenira u Dioklecijanovim podrumima ; (6.-8.2013.) prodaja sladoleda u kampu Medena ; (6.-9.) servirka u hotelu \'\'Resnik\'\' ; (8,-8.2015.)anketiranje i brojanje prometa- Institut IGH d.d.', 'Do sada sam sudjelovala na brojim Å¡kolskim projektima. Iskustvo koje sam stekla radeÄ‡i u timu te organiziranost Ä‡e uvijek biti od znaÄaja za moju buduÄ‡u osobnu i profesionalnu izgradnju.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(100, 'Ester ', 'Filipas', '1', 5, '2011', 'ester2.filipas@hotmail.com', '58d37471f3ae2095a793e9dba1cf3e0bae042e2f', 'Rijeka', 'znanje talijanskog, njemaÄkog i engleskog jezika, dobro snalaÅ¾enje na kompjuteru, zanima me sve u vezi prava', 'Cresanka d.d., raÄunovodstveni odjel, 8 ljetnih sezona odraÄ‘enih', 'nemam ih za sad', 9, 'blankuser.jpg', 'Yes', 'Da'),
(101, 'Barbara', 'Ä†elap', '2', 4, '2015.', 'bcelap22@gmail.com', '4f15baa639f1b950e6819e4298237d61d5a31074', 'Zagreb', 'Visok nivo poznavanja Å¡panjolskoga jezika i knjiÅ¾evnosti te hrvatskog jezika i knjiÅ¾evnosti uz upisan diplomski studij iz jezikoslovlja na obje studijske grupe; poloÅ¾en kolegij iz konsekutivnog prevoÄ‘enja (sa Å¡panjolskog na hrvatski); zanimaju me prevoÄ‘enje (sa Å¡panjolskog) i lektoriranje (na hrvatskom)', 'Nemam ga u struci', 'Zasada ih joÅ¡ uvijek nemam', 9, 'blankuser.jpg', 'Yes', 'Da'),
(102, 'Dorotea', 'RajÅ¡el', '8', 2, '2014.', 'dorotea.rajsel@gmail.com', 'ccd5f40f7a9dc63c398b5db7015fb8c38506cea2', 'Zagreb', 'Znam programirati u C, C++ i manje Python. OdliÄno komuniciram na engleskom jeziku. Marljiva, snalaÅ¾ljiva, odgovorna i voljna uÄiti, spremna na timski rad. Znam raditi na raÄunalu i koristiti MS Office. NajviÅ¡e me zanimaju podruÄja vezana uz matematiku, financije (primjenu matematike) i raÄunarstvo, programiranje. ', 'Doznaka i klupiranje u Hrvatskim Å¡umama (fiziÄki rad).', 'Dvaput sam sudjelovala na projektima na Festivalu znanosti vezanih uz matematiku u znanosti.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(103, 'Ana', 'RaguÅ¾', '2', 5, '2011', 'raguzana1991@gmail.com', '1cb643a74542d5b1ea61eef794f3e41249c387cc', 'Zagreb', 'StatistiÄka obrada podataka, intervju, psihologijsko testiranje,  istraÅ¾ivaÄki rad', 'Hendal market research\n', 'Plavi telefon\r\n', 9, 'blankuser.jpg', 'Yes', 'Da'),
(104, 'das', 'dasdasdas', '14', 3, '2013', 'svetlo_wrapcheboy@hotmail.com', 'c16bb7b60f35ab4e8b68329de6b9acb86732a7fb', 'Å ibenik', 'Business', 'das', 'dasd', 9, 'blankuser.jpg', 'Yes', 'Ne'),
(105, 'Matea', 'SrÅ¡en', '8', 2, '2013.', 'mateasrsen@hotmail.com', 'f31547ed4bb144cd34ef6835f30c7488b25b66df', 'Zagreb', 'NMR, IR, GC-MS,  organska sinteza', 'Rad u laboratoriju na Zavodu za Organsku kemiju PMFa', '-', 9, '1465473311.JPG', 'Yes', 'Da'),
(106, 'Luka', 'RukoniÄ‡', '7', 4, '2015', 'luka.rukonic78@gmail.com', '5a096fcceee62cb17c4118cd4450592ebf4f0327', 'Rijeka', 'Web development, Back-end, PHP, SQL, JavaScript', 'Student', 'WheeLabs, razni osobni projekti, Web aplikacije', 9, '1465474645.jpg', 'Yes', 'Da'),
(107, 'Katja', 'GrujiÄ‡', '11', 1, '2015./2016.', 'grujic.katja@gmail.com', '00e60a7bc0e8595ebd003d7e6d1b33579d5eae72', 'Zagreb', 'Computer Science, Programming, Software Development, C, Phyton,', 'Demonstratorica na kolegiju Digitalna logika, FER, SveuÄiliÅ¡te u Zagrebu, 10/2015 - 2/2016', 'U potrazi za novim i izazovnim projektima.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(108, 'David ', 'KriÅ¡tofiÄ‡', '11', 0, '2012/2013', 'david.kristofic@gmail.com', 'f805e54a501d0d0767d829f9432fc38089abb701', 'Zagreb', '- dobro poznavanje programskih paketa Matlab i Simulink, PLC programiranje - SIMATIC Simens, TIA Portal Simens, LabVIEW.\r\n\r\nKao student Fakulteta elektrotehnike i RaÄunarstva, smjer Automatika moji interesi leÅ¾e u automatizaciji industrijskih procesa. ', '- Demonstrator iz kolegija Elektronika 1, Fakultet elektrotehnike i raÄunarstva - 10/2013 - 02/2014\r\n- Demonstrator iz kolegija Osnovne elektotehnike, Fakultet elektrotehnike i raÄunarstva - 10/2012 - 02/2013', 'Diplomski seminar - Modeliranje i upravljanje AHU jedinicom. Cilj seminarskog zadatka je upoznati s HVAC sustavima te modeliranje AHU jedinice u svrhu     pametnog upravljanja na kasnijim projektima. \n\nPLC+ - StruÄna radionica Eestec udruge odrÅ¾ana na FER-u. RjeÅ¡avanje zadatka programiranja dijela procesa pivovare u programskom pakteu TIA Portal. \n\nRobosort - Cilj projektnog zadatka je izrada robota za sortiranje predmeta prema boji. Sustav se sastoji od troosnog SCARA (T-RR) manipulatora s magnetom u prihvatnom mehanizmu, podruÄja za prihvat predmeta te podruÄja za odlaganje crvenih i plavih predmeta.\n\nZavrÅ¡ni rad - Integracija senzora sile u sustav upravljanja kartezijskog robota Schneider electric. \n', 9, 'blankuser.jpg', 'Yes', 'Da'),
(109, 'Luka', 'Demarin', '1', 4, '2012.', 'luka.demarin@gmail.com', '08e1dfb849b3814e52a8d0318ddccb4272d01562', 'Split', 'Pijanist i buduÄ‡i profesor klavira i glazbenoteorijskih predmeta.', 'Nema', 'Nema', 9, 'blankuser.jpg', 'Yes', 'Da'),
(110, 'Marina', 'MikuliÄ‡', '10', 4, '2011./2012.', 'marina.mikul@gmail.com', '$2y$10$8L5e0N212HKY0gPy21YdCeFtWNKiOAbi1eLH8Y1xhnVQPQeczLvc6', 'Zagreb', ' interes za trgovaÄko pravo , meÄ‘unarodno pravo', 'Volonter u Pravnoj klinici Pravnog fakulteta u Zagrebu', 'ÄŒlan Kluba mladih izmiritelja', 9, 'blankuser.jpg', 'Yes', 'Da'),
(111, 'Matija', 'Banjan', '7', 3, '2012', 'matija.banjan@uniri.hr', 'cf56ef4bb76aa209cdd4181fa64832bf4e0a52af', 'Rijeka', ' ', ' ', ' ', 9, 'blankuser.jpg', 'Yes', 'Ne'),
(112, 'Filip', 'DmitraÅ¡inoviÄ‡', '1', 4, '2012', 'filipdmitrasinovic@yahoo.com', '2bedb50b95ba981563092f6893fc43f63a230d6b', 'Rijeka', 'PoloÅ¾en opÄ‡i teÄaj intelektualnog vlasniÅ¡tva, DL-101 WIPO e-learning centre. Dobitnik dekanove nagrade za akademsku godinu 2014/2015 (prosjek 4.7). Demonstrator na kolegiju Gradjansko pravo. U akademskoj godini 2015/2016, u okviru kolegija Klinika za gradjansko pravo, odradio sam dvomjeseÄnu praksu na OpÄ‡inskom sudu u Rijeci.\r\nMoji glavni interesi se odnose na: europsko pravo, pravo EU, pravo intelektualnog vlasniÅ¡tva, trgovaÄko pravo, gradjansko pravo, pravo druÅ¡tava i medunarodno pravo.', 'OÅ¾ujak-Lipanj, 2016 godine. praksa na OpÄ‡inskom sudu u Rijeci.', 'Seminarski radovi u okviru kolegija ustavno pravo, kazneno pravo, gradjansko pravo, gradjansko postupovno pravo te trgovaÄko pravo.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(113, 'Filip', 'DmitraÅ¡inoviÄ‡', '1', 4, '2012.', 'filip.dmitrasinovic62@gmail.com', '2bedb50b95ba981563092f6893fc43f63a230d6b', 'Rijeka', 'U akademskoj godini 2015/2016., u okviru kolegija Klinika za graÄ‘ansko pravo, izvrÅ¡avao sam dvomjeseÄnu praksu na OpÄ‡inskom sudu u Rijeci.\r\n2016. poloÅ¾io sam OpÄ‡i teÄaj intelektualnog vlasniÅ¡tva, DL-101 WIPO Academy e-learning centre. Dobitnik dekanove nagrade, za ostvareni prosjek 4.7, u akademskoj godini 2014/2015. Jezici: hrvatski, engleski i talijanski. Moji glavni interesi su usmjereni na europsko pravo, pravo EU, meÄ‘unarodno javno i meÄ‘unarodno privatno pravo, pravo intelektualnog vlasniÅ¡tva, trgovaÄko pravo, pravo druÅ¡tava te graÄ‘ansko pravo. Od 2014. godine izvrÅ¡avam funkciju demonstratora na kolegiju graÄ‘ansko pravo.', 'oÅ¾ujak-svibanj, 2016. praksa na OpÄ‡inskom sudu u Rijeci', 'Seminarski radovi u okviru slijedeÄ‡ih kolegija: ustavno pravo, kazneno pravo, graÄ‘ansko pravo, trgovaÄko pravo, financijsko pravo, graÄ‘ansko postupovno pravo.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(114, 'Sanja', 'KovaÄiÄ‡', '2', 5, '2012.', 'sanja.kovacic@unicath.hr', '4b640e57b9413a06cfabfb2c55d9ce0eb91a976a', 'Zagreb', 'kliniÄka psihologija, organizacijska psihologa, savjetovanje', 'promocije, harm reduction program, OKLA VrapÄe', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(115, 'Ivona', 'KovaÄiÄ‡', '3', 3, '2013.', 'ivna.kovacic@gmail.com', '088878b41bb6d91a1fd8d3af16b903af4ae274d7', 'Zagreb', 'AutoCAD, SAP', '-', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(116, 'Valentina', 'Å oÅ¡tariÄ‡', '14', 0, '2014. godina', 'valentinasostaric09@gmail.com', '3a02a61fde21f9121edf825f2e7d4dd555933afe', 'ZapreÅ¡iÄ‡ / OpÄ‡ina Brdovec', 'Otvorena, vedra, komunikativna i spremna za timski rad; upoznata s radom na raÄunalu te radu s ljudima\r\nZanimam se za marketinÅ¡ki menadÅ¾ment, rad u turizmu, na beauty trÅ¾iÅ¡tu i trÅ¾iÅ¡tu nekretninama\r\nHobiji : ZavrÅ¡ena glazbena Å¡kola - odjel gitare / TeÄaj Å¡panjolskog i njemaÄkog jezika / Rekreativno bavljenje plesom i fitnessom, jogging', 'Pozivni referent u tvrtki za naplatu dugova Prima Solvent d.d.\r\nLektorica u lokalnim novinama\r\nRecepcionarka u Solaris Beach resortu te teretanama', 'Znanstvenih radova nemam, pod projektima smatram volontiranje u radu s izbjeglicama, rad na gospodarskom sajmu i lokalnim izborima, suradnja na lektoriranju lokalnih mjeseÄnih novina..', 9, 'blankuser.jpg', 'Yes', 'Da'),
(117, 'Tihana', 'Hajdinjak', '1', 3, '2013.', 'tihana.hajdinjak@gmail.com', '4d8251b9c1d4dc9a71a500823bd44086797259f2', 'ÄŒakovec', 'Dizajn interijera, 3d vizualizacija, fotografija', 'Konstruktivan d.o.o. VaraÅ¾din ( projektiranje i  dizajn)\r\nDanfoss trata Ljubljana (proizvodnja)', 'Projekt interiera kuÄ‡e u MengeÅ¡u (Ljubljana)\r\nProjekt interiera kuÄ‡e i vanjske fasade u VaraÅ¾dinu', 9, 'blankuser.jpg', 'Yes', 'Da'),
(118, 'Ivana', 'Medved', '14', 1, '2015', 'ivana.medved16894@gmail.com', 'e22bbad19f68ba91d40c58a16c14f9fc58847626', 'Rijeka', 'Poznavanje rada u Mc Office, znanje stranih jezika: engleski, njemaÄki, Å¡panjolski. Interes: financije i raÄunovodstvo', 'Studentski poslovi', 'Start up projekt \"Index takeout\" u sklopu kolegija marketinga', 9, 'blankuser.jpg', 'Yes', 'Da'),
(119, 'Vicenco', 'TomaÅ¡', '7', 3, '4.', 'vicenco.toma@gmail.com', 'c707b85cf8ebcc7645482f73789e45433eabebdf', 'Rijeka', 'Programiram u C++ i Pythonu. Zanimaju me embedded sustavi.', 'Nemam radnog iskustva u struci.', 'Jednostavna aplikacija izraÄ‘ena u Clarionu.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(120, 'Lea', 'KosoviÄ‡', '13', 3, '2013.', 'lea.kosovic@gmail.com', 'f496e64cde616b14624f082c8f6b6343c82567db', 'Zagreb', 'VjeÅ¡tine: organizacija dogaÄ‘aja, odrÅ¾avanje druÅ¡tvenih mreÅ¾a,, planiranje projekta, pisanje Älanaka za interne Äasopise, pisanje priopÄ‡enja, kontaktiranje medija...\r\nKompetencije: timski rad, komunikativnost, kreativnost, organizacijske i prezentacijske vjeÅ¡tine, strpljivost, razmiÅ¡ljanje \"out of the box\"...\r\nInteresi: organizacija dogaÄ‘aja, marketing, odnosi s javnoÅ¡Ä‡u, govorniÅ¡tvo, struÄne edukacije...', 'Mangonel d.o.o (2015. - trenutno; PR & Communications Consultant)\r\nNabava.net (8 mjeseci; voÄ‘enje druÅ¡tvenih mreÅ¾a, brand awareness, organizacija press konferencije)\r\nIntegra znanja d.o.o. (godina i Äetiri mjeseca; asistent ljudskih resursa)\r\nM+ International Services (2013 - 2014; asistent za odnose s javnoÅ¡Ä‡u)\r\nJungle d.o.o. (2011 - trenutno; instruktorica latino-ameriÄkih i standardnih plesova) ', 'Projekti:\r\nPopUpGarden 2016. - organizacija PopUpGarden u sklopu Fantastic Film Festivala na dvije lokacije (TuÅ¡kanac, Medvedgrad)\r\nFloraart 2016. - Kontinuirano odrÅ¾avanje druÅ¡tvenih mreÅ¾a prije i tijekom sajma (Facebook, Instagram) s nekoliko objava dnevno. (pismena preporuka)\r\nRexpo 2015. - Sudjelovanje u organizaciji Äetvrtog meÄ‘unarodnog sajma investicijskih projekata - Rexpo 2015. Sajam okuplja domaÄ‡e i strane ulagaÄe te predstavlja najbolje investicijske projekte u regiji. (pismena preporuka)\r\nBiznisPlus (oÅ¾ujak 2014.) - sudjelovanje u organizaciji regionalnog poslovnog susreta Biznis plus u organizaciji izdavaÄke kuÄ‡e Novem Publishing\r\n\r\n', 9, 'blankuser.jpg', 'Yes', 'Da'),
(121, 'Anamarija ', 'Dotur', '14', 1, '2016/2017', 'anamarija_dotur@hotmail.com', '838c55a6f8bcb056bf14e697a095000a401a7b14', 'Sinj', 'Izvrsno poznavanje engleskog jezika, solidno znanje talijanskog.  Rad u MS Officu. ', 'Rad u ugostiteljstvu ', 'Nista', 9, 'blankuser.jpg', 'Yes', 'Da'),
(122, 'Tomislava', 'KrstiÄeviÄ‡', '2', 3, '2013.', 'tomis.krsticevic@gmail.com', 'c1868c156b4d8912d550a2fd5a7a15a4a9fae6c4', 'Split', 'Aktivno poznavanje engleskog jezika, pasivno poznavanje njemaÄkog, rad u Microsoft Office-u, lektura, korektura.\r\nPisanje i glazba.', 'rad na lokalnim izborima, babysitting, telefonsko i terensko anketiranje (Ipsos Puls), obuka za rad na blagajni (InterSpar City Center Split), inventura (Kaufland Split)', '-', 9, '1469790212.jpg', 'Yes', 'Da'),
(123, 'Lucija', 'Hrsak', '5', 5, '2011', 'hrsak.lux@gmail.com', 'afb54d86033524c094267c824f729682eef06f5a', 'Krapina', 'Postovani, studentica sam prehrambeno-biotehnoloskog fakulteta,smjer nutricionizam..tijekom studija stekla sam znanja iz kemije,laboratorijski rad I analiza hrane,mikrobiologije,matematike,anatomije,fiziologije covjeka,te prehrane razlicitih dobnih I specificnih skupina!', 'Promocije u ljekarnama, volonterski rad u KBC-Rebro, sudjelovanje na projektu u Klaicevoj bolnici', 'Projekti: optimizacija otpada klinicke prehrane, \'Nutrition day\'-Klaiceva bolnica, odrzavanje dana osoba oboljelih od celijakije', 9, 'blankuser.jpg', 'Yes', 'Da'),
(124, 'Natalija', 'Å pruk', '2', 5, '2011.', 'natalija.spruk@gmail.com', 'eedb1872b522d3ba29c1c5260355d574502d7671', 'Zagreb', 'OdliÄno snalaÅ¾enje na Internetu i sa Office paketom, vozaÄka B kategorije, vrlo dobro poznavanje engleskog jezika, vrlo dobre komunikacijske vjeÅ¡tine (zavrÅ¡en teÄaj govorniÅ¡tva i komunikacijske psihologije u trajanju od 10 sati), vrlo dobre organizacijske vjeÅ¡tine, pismenost', 'Nekoliko kratkoroÄnih studentskih poslova u Kauflandu, Zvijezdi i Htvatskoj poÅ¡ti, te dugoroÄno u HT-u, godinu i 8 mjeseci u  sluÅ¾bi za korisnike, te centru za savjetovanje i zadrÅ¾avanje korisnika. Trenutno radim u Vipnetu, viÅ¡e od dvije godine u odjelu za savjetovanje korisnika za B.net korisnike.', 'StruÄna praksa odraÄ‘ena u KnjiÅ¾nicama grada Zagreba, u KnjiÅ¾nici Augusta Cesarca,', 9, 'blankuser.jpg', 'Yes', 'Da'),
(125, 'Ana', 'JuzbaÅ¡iÄ‡', '2', 2, '2015.', 'ana.juzzz7@gmail.com', '68a459ebf46f779554abe5df60fe1968de7b88db', 'Osijek', 'Engleski jezik\r\nNjemaÄki jezik\r\nTurizam i komunikacije', '0', '0', 9, 'blankuser.jpg', 'Yes', 'Da'),
(126, 'AnÄ‘ela', 'KlisoviÄ‡', '7', 2, '2014', 'andela.klisovic10@gmail.com', '30512b7ffaaa45bd6a455d93ebd23e0d5f2d5747', 'Å ibenik', 'OS Windows, programiranje u jezicima C++ , C#, HTML, CSS , PHP , JS, Internet servisi i usluge, vjeÅ¡to baratanje Microsoft Office (Word, Excel, PowerPoint) alatimaâ€‹, dobro upravljanje CMS sustavom steÄeno radom u novinarskoj sekciji veleuÄiliÅ¡ta. \r\ndrugi jezici: engleski jezik - B2.\r\nDobra komunikativna sposobnost, otvorenost, fleksibilnost, kreativnost, timski duh, te Å¾elja i sposobnost za stavljanjem dobrobit tima ispred vlastitih interesa. \r\nVolim fotografirati, glazbu, Å¡etnju,\r\nputovanja, rad na raÄunalu, internet', 'Priprema i usluÅ¾ivanje hrane u ugostiteljstvu u Fast food-u Silvija od rujna 2013  â†’\r\nNovinarka Studestkog kluba VeleuÄiliÅ¡ta u Å ibeniku od studenog 2015  â†’', 'Sudjelovanje na projektu â€žStudirajmo zajedno!â€œ Ekonomskog fakulteta u Splitu od kolovoza 2013. do veljaÄe 2015.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(127, 'Robert', 'MiÄ‡anoviÄ‡', '11', 5, '2010.', 'mics.robert@gmail.com', 'ba6cb9bf2b00da710cf3a7ee7d61fd8484afed82', 'Zagreb', 'vjeÅ¡tine: CES EduPack, MS Office, Creo, Engleski jezik (odliÄno), Talijanski (poÄetno)\r\ninteresi: inÅ¾enjerstvo materijala, nanotehnologije, strojarstvo', 'Praksa: Metris (Pula), Tehnomont (Pula), razliÄiti studentski poslovi', 'Radovi: \"Drvno-plastiÄni kompoziti\" - Fakultet strojarstva i brodogradnje u Zagrebu, 2015. \r\nProjekti: sudjelovanje na simpoziju studenata kemiÄara u Zagrebu s navedenim radom, Zagreb 2015.\r\n                 sudjelovanje na meÄ‘unarodnoj konferenciji za materijale i tribologiju MATRIB, KoruÄula 2016.\r\n                 sudjelovanje na internacionalnom seminarskom projektu BEST udruge: \" Explore Design and Touch the Nanoworld\", Brno, ÄŒeÅ¡ka, 2015. ', 9, 'blankuser.jpg', 'Yes', 'Da'),
(128, 'Ivan', 'Tovili', '11', 1, '2015', 'ivan.tovilo95@gmail.com', '9506e1c2fc4b5be6ac35a300604d4b80a34cfa61', 'Zagreb', 'VozaÄka b kategorije, zanima me sve vezano uz promet i automobile.', 'Srpanj Â Â â€“ kolovoz 2014. - SkladiÅ¡te, ljepljenje deklaracija, Â dostava â€“ Narodne Novine\r\n\r\nÂ·Â Srpanj Â â€“ Â kolovoz 2015. â€“ Aquapark Â Å imuni, Pag, blagajnik , rad s ljudima\r\n\r\nÂ·Â 2015. â€“ rad na istovaru namjeÅ¡taja po cijelom Zagrebu za tvrtku Tvin d.o.o., Virovitica \r\n\r\nÂ·Â Travanj â€“ svibanj Â 2016. â€“ rad u Oryx rent-a-caru, VIP Â voÅ¾nje\r\n\r\nÂ·Â Svibanj â€“ srpanj 2016. â€“ Â Sixt rent a car\r\n', 'Bez projekata', 9, 'blankuser.jpg', 'Yes', 'Da'),
(129, 'Liana', 'GrubiÄ‡', '13', 5, '2016.', 'lianagrubic@gmail.com', '2ffea57e6ea295df0169cdc3dea1538aa4e7c4d5', 'Zagreb', 'Komunikacijske, vjeÅ¡tine organizacije, raÄunalne vjeÅ¡tine u grafiÄkim programima i smjera menadÅ¾erske informatike, interes za uredske i informatiÄke poslove. Timski rad.', '08.09.2016. â€“ 10.09.2016. Ipsilon d.o.o.\n14.09.2016. â€“ 15.09.2016. â€“ distribucija letaka\n25./ 26.05.2016. Serena pro d.o.o. (http://serenapro.hr/ )\n- statiranje u seriji â€œCrno-bijeli svijetâ€.\n20.02.2016. Kaufland Hrvatska K. D. Zagreb\n- inventura.\n10.09.2015. - 20.09.2015. Distribucija letaka\n2009. â€“ 1.09.2015. Grafika Helvetica ( http://www.grafikahelvetica.com/ )\n- poslovi u doradi, rad na grafiÄkim programima, administrativni poslovi.\n06.02.2015. Kaufland Hrvatska P. J. Rijeka\n- inventura.\n2010. â€“ 2013. Novi list d. d.\n- poslovi u doradi (ubacivanje priloga u Novi list).', 'Case Study Competition, Job Hunt.', 9, '1475346863.jpg', 'Yes', 'Da'),
(130, 'Ana', 'RaguÅ¾', '2', 5, '2011', 'raguzanaa1991@gmail.com', '1cb643a74542d5b1ea61eef794f3e41249c387cc', 'Zagreb', 'Psihologijsko testiranje i inervju, obrada podataka u spss, interes za podruÄje ljudskih resursa', 'Hendal Market Researh-administrativni poslovi, statisticka obrada podatala', 'Ljetna psihologijska Å¡kola ( utjecaj medija na socioemocionalni razvoj mladih)', 9, 'blankuser.jpg', 'Yes', 'Da'),
(131, 'Filip ', 'Jugkala', '2', 2, '2015.', 'filip.jugkala@yahoo.com', '634417e1ebc780a9038988d7f34a926100a39e47', 'Zagreb', 'Druga godina na studiju informacijskih znanosti.\r\nRad u MS Office-u.\r\nOsnove HTML, Python .', '\"Pliva Hrvatska\" -  radnik u proizvodnji\r\n\"ZOO Hobby\" - pomoÄ‡ni radnik u skladiÅ¡tu\r\n* Honorarni poslovi                  Dijeljenje letaka i sezonski rad na plaÅ¾i (Restoran â€žGladne oÄiâ€œ). \r\n* Studentski poslovi                 Djelatnik u kinu (Cinestar), pomoÄ‡ pri selidbi karantene(ZOO Hobby).                                                                                                                                                                           \r\n', 'â€¢ Postavljen kao antidopinÅ¡ki sportski ambasador u Erasmus + projektu pod nazivom â€žprePLAYâ€œ.\nâ€¢ Volontirao na Äetiri kvalifikacijske utakmice u suradnji s â€žHrvatskim nogometnim savezomâ€œ.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(132, 'Bernarda', 'ÄŒernauÅ¡', '5', 1, '2016.', 'bernardacernaus@gmail.com', 'f721cbd8f623acd8ab230ef0e5cc249ee98ccf79', 'Samobor', '.', '.', '.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(133, 'Morena', 'DevÄiÄ‡', '13', 2, '2', 'morena.devcic93@gmail.com', '2fd160458bc0036381e0e3605720c78d052687aa', 'Zagreb', 'Imam iskustva u radu s ljudima (direktna i telefonska prodaja). ', 'Iskustvo u trgovini, call centru, u ugostiteljstvo, animacija djece...', 'Projekt na komunikacijskom planu - promoviranje Udruge itd', 9, 'blankuser.jpg', 'Yes', 'Da'),
(134, 'Marija', 'CrÄiÄ‡', '14', 5, '2012', 'marijacrcic93@net.hr', '06deb19bd55e860552f7c8cbdce43d33ab635feb', 'Zagreb', 'RaÄunovodstvo i revizija, financije, administrativni poslovi, ljudski resursi, marketing, poslovno savjetovanje', 'Razni studentski poslovi poput administrativnih poslova ili poslova ÄiÅ¡Ä‡enja', 'Z', 9, 'blankuser.jpg', 'No', 'Da'),
(135, 'Marija', 'CrÄiÄ‡', '14', 5, '2012', 'mcrcic@net.efzg.hr', '06deb19bd55e860552f7c8cbdce43d33ab635feb', 'Zagreb', 'RaÄunovodstvo i revizija, financije, administrativni poslovi, ljudski resursi, marketing, poslovno savjetovanje', 'Razni studentski poslovi poput administrativnih poslova ili poslova ÄiÅ¡Ä‡enja', 'Analiza konkurentnosti Irske i Portugala, MarketinÅ¡ki plan za projekt \"Urbani vrtovi Zagreba\", Analiza Äimbenika oblikovanja organizacije poduzeÄ‡a \"Pik Vrbovec d. d.\" , Porez na dobit, MarketinÅ¡ka analiza poduzeÄ‡a \"Brenta d.o.o.\"', 9, 'blankuser.jpg', 'Yes', 'Da'),
(136, 'Veronika', 'GrozdaniÄ‡ ', '7', 0, '2016/2017', 'Veronikagrozdanic@gmail.com', 'f76bcec31279865d6f6a93ed279fb4d0c0e06a29', 'Rijeka', '/', 'Rad u mnogim firmama ', '/', 9, 'blankuser.jpg', 'Yes', 'Da'),
(137, 'Gdfh', 'Fhkf', '11', 1, '2016', 'jufdjtfic@gmail.com', '12ca1dbe3a48e066a3fdbce83c2c070530838ab5', 'Zagreb', 'Python, django, js, html, css, c', 'Nema', 'Nekoliko manjih stranica, dostupnr na githubu', 9, 'blankuser.jpg', 'Yes', 'Da'),
(138, 'Mirna', 'Papuga', '2', 5, '2012', 'mirna.papuga@gmail.com', 'd5403cd74ec12bef015b31371b67fc4d71f33ae0', 'Zagreb', 'Studentica 5. godine prevoditeljskog smjera ruskog i francuskog jezika. VjeÅ¡tine i interesi: usmeno i pismeno prevoÄ‘enje, poduÄavanje, lektoriranje, voÄ‘enje turistiÄkih tura te turizam opÄ‡enito, poslovi interaktivnog i kreativnog karaktera ', 'srpanj â€“ rujan 2016:  projekt Obonjan Rivijera  http://www.obonjan-island.com/  \r\nâ€¢	recepcija \r\n\r\nsrpanj 2016: Hideout festival\r\nâ€¢	artist liaison\r\n\r\nsrpanj â€“ rujan 2015: supermarket Konzum, Å ibenik \r\nâ€¢	prodavaÄica\r\n\r\nsrpanj â€“ rujan 2014: Carpe Diem entertainment company, Solaris Beach Resort http://www.carpe-diem.hr  \r\nâ€¢	turistiÄka animatorica\r\n\r\nsrpanj â€“ rujan 2012 i 2011: Galerija od spuÅ¾ava i suvenirnica Å½itak, otok Krapanj               \r\nhttp://www.zitak.hr/hr/galerijaodspuzava.html \r\nâ€¢	prodavaÄica\r\n\r\nhonorarno, 2011 i 2010: MeÄ‘unarodni djeÄji festival: MDF Å ibenik http://www.mdf-sibenik.com/\r\nâ€¢	asistent turistiÄkom vodiÄu, informator\r\n\r\n\r\n', '/', 9, 'blankuser.jpg', 'Yes', 'Da');
INSERT INTO `student` (`id`, `first_name`, `last_name`, `university`, `current_year`, `year_of_study`, `email`, `password`, `domicle`, `hobby`, `experience`, `projects`, `fk_category`, `fk_employer`, `Active`, `Taken`) VALUES
(139, 'Marijana', 'Kalanj', '13', 3, '1', 'cromarijuana@gmail.com', '125b7dc556f0fe61ad858a57931b13e0d2b15e28', 'Rijeka', 'Rad s bazom podataka my sql, znanje programskih jezika(c, c++, java), Å¾elja za uÄenjem ', 'Strucna praksa ', '-', 9, 'blankuser.jpg', 'Yes', 'Da'),
(140, 'Ivan', 'Horvat', '3', 2, '2014', 'nkhvffkg@sharklasers.com', '6e7a77e0b1ee1eef37face9be9ff746c04c2d6a2', 'Zagreb', 'Dosta toga', 'Odkad znam za sebe', 'Sve', 9, 'blankuser.jpg', 'Yes', 'Da'),
(141, 'Ivan', 'Horvat', '3', 2, '2014', '5y3fvo+5moy0qa9wmqx4@sharklasers.com', '6e7a77e0b1ee1eef37face9be9ff746c04c2d6a2', 'Zagreb', 'Dosta toga', 'Odkad znam za sebe', 'Sve', 9, 'blankuser.jpg', 'Yes', 'Da'),
(142, 'Luka', 'GariÄ‡', '7', 2, '2015', 'lgaaric@gmail.com', 'de948bb292a84c64a9d184f1573dabff75b9c541', 'VaraÅ¾din', 'Tijekom studiranja stekao sam iskustva s Python i C++ programskim jezicima. Upoznat sam s Access-om te izradom baze podataka. Uz informatiÄki sektor imam iskustva u modeliranju poslovnih procesa u BPMN programu.\r\nBrzo i lako usvajam nove vjeÅ¡tine te sam uzbuÄ‘en kada mi se pruÅ¾i prilika da nauÄim neÅ¡to novo. U poslu dajem sve od sebe.', 'Nemam radnog iskustva u struci.\r\n', 'Modeliranje poslovnog procesa firme Solmex d.o.o.\r\n4 seminarska rada u sklopu fakulteta', 9, 'blankuser.jpg', 'Yes', 'Da'),
(143, 'Jana', 'MaljiÄ‡', '14', 0, '2012', 'janamalja@gmail.com', '032ac73cc9dd4c696842cda57ae7adf5718cea2a', 'Pula', 'Organizacija, management, marketing, meÄ‘unarodno poslovanje, poslovni engleski, Microsoft Office, komunikacija, timski rad, prezentacija, project management', 'Internship u CMGC, agenciji za digitalni marketing u Puli, Lipanj 2015 â€“ Rujan 2015\r\n', 'Bila sam Älanica AIESEC - a, studentske organizacije za prakse gdje sam volontirala u timu za odlazne prakse, organizirala sam intervjue sa studentima i odrÅ¾avala info meetinge.', 9, 'blankuser.jpg', 'Yes', 'Da'),
(144, 'Ana', 'Panjako', '9', 3, '2014', 'ana.panjako95@gmail.com', '3cb733309f991c94fe7a827e139a29a916c113b3', 'Rijeka', 'Komunikativna, marljiva te otvorena osoba, spremna za induvidualni rad ili za rad u timu. Spremna sam usvajati nova znanja i vjeÅ¡tine. Nadam se radu u pozitivnom okruÅ¾enju gdje postoji moguÄ‡nost rada u struci te napredovanja.', '- studentski poslovi u duÄ‡anima s robom: H&M, Mango, Marks & Spencer, s.Oliver\r\n- prodajni predstavnik na raznim promocijama za agenciju MPG', '-', 9, 'blankuser.jpg', 'Yes', 'Da');

-- --------------------------------------------------------

--
-- Table structure for table `universities`
--

CREATE TABLE `universities` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `universities`
--

INSERT INTO `universities` (`id`, `name`) VALUES
(1, 'Akademija primjenjenih umjetnosti'),
(2, 'Filozofski fakultet'),
(3, 'Gra&#273;evinski fakultet'),
(4, 'Medicinski fakultet'),
(5, 'Odjel za biotehnologiju'),
(6, 'Odjel za fiziku'),
(7, 'Odjel za informatiku'),
(8, 'Odjel za matematiku'),
(9, 'Pomorski fakultet'),
(10, 'Pravni fakultet'),
(11, 'Tehni&#269;ki fakultet'),
(12, 'U&#269;iteljski fakultet'),
(13, 'Razno'),
(14, 'Ekonomski fakultet'),
(15, 'Visoka poslovna škola PAR');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `universities`
--
ALTER TABLE `universities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `employer`
--
ALTER TABLE `employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `universities`
--
ALTER TABLE `universities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
